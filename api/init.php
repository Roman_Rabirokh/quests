<?php
session_start();
ini_set("display_errors","on");
define("_APP_START",TRUE);
include_once("config.php");
include_once(_DIR . "api/db.php");
include_once(_DIR . "api/functions.php");

include_once(_DIR . "api/image.php");
include_once(_DIR . "api/user.php");
include_once(_DIR . "api/application.php");
include_once(_DIR . "api/page.php");

$app = new APP();
$page = new Page();
$user = new User();

if(isset($modules))
{
	foreach($modules as $module)
	{
		include(_DIR . "modules/" . $module . ".php");
	}
}

$result = FALSE;

ob_start();

$app->processRouting();

if(!$app->actionResult)
{
	$app->IncludeComponent("system/content.page",array("URL"=>"404"));
	$page->notFound = TRUE;
}

$content = ob_get_contents();

ob_end_clean();

if(!$app->actionResult)
{
	get404();
}

$app->processTemplate($content);
