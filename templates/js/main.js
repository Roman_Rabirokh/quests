
$(document).ready(function ()
{

    scrollDown();
    $('.remove').on('click', function () {
        var divid = $(this).attr('divid');
        $.post('./delete/invoice',
                {qid: divid},
                function (data) {
                    console.log(data);
                    if (data.status == 1) {
                        alert('Квест удален из избранного!');
                    }
                });
        // alert(divid);
        $("#" + divid).hide();
    });

    if ($("div").is('#timer'))
        timer();
    $(function () {
        $("#datepicker").datepicker();
    });
    var offset = 0;
    $('#showMoreQuests').click(function ()
    {
        offset += 1;
        $.ajax
                ({
                    url: '/list/quests',
                    type: 'POST',
                    data: {offset: offset},
                    success: function (data)
                    {
                        $('.row.block').last().after(data);
                        $('button.btn.btn-md.btn-default.clear').click(btnClickDeeleQuest);

                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        alert('статус:' + jqXHR.readyState + "  " + jqXHR.status + "    " + jqXHR.responseText + "   ");
                        alert("Что-то пошло не так. Повторите попытку позже! " + textStatus + " " + errorThrown + " " + jqXHR);
                    }
                });
    });
    $('button.btn.plus').click(bntClickAddLevel);

    $('button.btn.minus').click(btnClickDeleteLevel);


    $('.btn.add-q').click(function ()
    {
        $('input[name=active]').val(1);
    });
    $('.btn.chern').click(function ()
    {
        $('input[name=active]').val(0);
    });


    $('form[name=form_quest]').submit(function ()
    {
        var ajax=  $('input[name=no_ajax]').val();
        if(ajax=="0")
        {   
            save_quest();
            return false;
        }
        else
            return true;
//        if (!$('input[name=name_quest]').val())
//        {
//            alert('Введите название квеста');
//            return false;
//        }
//        if (!$('textarea[name=detail_text]').val())
//        {
//            alert('Введите описание квеста');
//            return false;
//        }
//    if(!$('input[name=time_quest]').val())
//    {
//      alert('Введите среднее время');
//      return false;
//    }
//    if(!$('input[name=people_quest]').val())
//    {
//      alert('Введите кол-во участников');
//      return false;
//    }
//    if(!$('input[name=length_quest]').val())
//    {
//      alert('Введите растояние');
//      return false;
//    }
//    if(!$('input[name=price_quest]').val())
//    {
//      alert('Введите цену');
//      return false;
//    }
//    if(!$('input[name=start_route_quest]').val())
//    {
//      alert('Введите начало маршрута');
//      return false;
//    }
//    if(!$('input[name=time_start]').val())
//    {
//      alert('Введите время игры: от');
//      return false;
//    }
//    if(!$('input[name=time_end]').val())
//    {
//      alert('Введите время игры: до');
//      return false;
//    }
//    if(!$('input[name=location]').val())
//    {
//      alert('Введите "Как добраться?"');
//      return false;
//    }

//        var error = 0;
//        $(".levels").each(function (index)
//        {
//            var temp;
//            temp = $(this).find('#name_level').val();
//            if (!temp && temp !== undefined)
//            {
//                alert('Введите название квеста');
//                error = 1;
//                return false;
//            }
//            temp = $(this).find('#task_level').val();
//            if (!temp && temp !== undefined)
//            {
//                alert('Введите задание квеста');
//                error = 1;
//                return false;
//            }
//            temp = $(this).find('#answer_level').val();
//            if (!temp && temp !== undefined)
//            {
//                alert('Введите ответ квеста');
//                error = 1;
//                return false;
//            }
//            temp = $(this).find('.time_hint_3').val();
//            if (!temp && temp !== undefined)
//            {
//                alert('Введите время 3 подсказки.');
//                error = 1;
//                return false;
//            }
//        });
//        if (error === 1)
//            return false;
//        else
//            return true;

    });

    $('#button_add_game').click(function () {
        ajaxCreateInvoice($(this));
    });

    $('#save_quest').click(function ()
    {
        $('input[name=no_ajax]').val(1);
        $('form[name=form_quest]').submit();
    });
    
    $('.input-form-quest').change(function()
    {
       $('form[name=form_quest]').submit(); 
    });
    
    $('.mt-content').on('click', '.public_quest', function ()
    {
        var id = $(this).attr('qid');
        var button = $(this);
        $.ajax
                ({
                    url: '/publish/quest/',
                    type: 'POST',
                    data: {id: id},
                    dataType: 'json',
                    success: function (data)
                    {
                        if (data.status = 'success')
                        {
                            button.after('<div class="text_wborder">Отправлен на модерацию</div>');
                            button.remove();

                        }

                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        alert('статус:' + jqXHR.readyState + "  " + jqXHR.status + "    " + jqXHR.responseText + "   ");
                        alert("Что-то пошло не так. Повторите попытку позже! " + textStatus + " " + errorThrown + " " + jqXHR);
                    }
                });


    });

    $('.send_gift').click(function ()
    {
        var idGame = $(this).attr('gid');
        var div = $(this).parent().find('#gift_code');
        var button = $(this);
        var button_buy = $(this).parent().parent().find('.btn.btn-md.btn-default.edit');
        $.ajax
                ({
                    url: 'game/get-code/',
                    type: 'POST',
                    data: {idGame: idGame},
                    dataType: 'json',
                    success: function (code)
                    {
                        if (code)
                        {
                            div.find('span').empty();
                            div.find('span').append(code);
                            div.show();
                            button.remove();
                            button_buy.remove();
                        }

                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        alert('статус:' + jqXHR.readyState + "  " + jqXHR.status + "    " + jqXHR.responseText + "   ");
                        alert("Что-то пошло не так. Повторите попытку позже! " + textStatus + " " + errorThrown + " " + jqXHR);
                    }
                });


    });

    $('button.btn.btn-md.btn-default.clear').click(btnClickDeeleQuest);
    $('#delete_image').click(delete_image_quest);

    $('#delete_file').click(delete_sound_file);

    $('#button_select_image').click(function ()
    {
        $('input[name=main_image]').click();
    });


    $('#b_big_image').click(function ()
    {
        $('#big_image').click();
    });
    $('input[name=big_image]').change(function () {

        var file_data = $(this).prop("files")[0];   // Getting the properties of file from file field
        var form_data = new FormData();                  // Creating object of FormData class
        form_data.append("file", file_data);              // Appending parameter named file with properties of file_field to form_data
        var id = $('input[name=questID]').val();
        form_data.append("id", id);
        $.ajax
                ({
                    url: '/ajax/save/big-image/',
                    dataType: 'script',
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: form_data, // Setting the data attribute of ajax with file_data
                    type: 'post',
                    success: function (data)
                    {
                        console.log(data);
                        console.log(JSON.parse(data));
                        var image = JSON.parse(data);
                        $('#big_image_div').css('background', 'url(https://quest.dg-star.com/templates/img/bg/header-3.png) center center, url(' + image + ') no-repeat center top');
						 $('#big_image_div').css('background-size', 'contain, 1100px auto');
                        $('#big_image_div').css('z-index', '99');
                        $('#b_big_image').html('ИЗМЕНИТЬ ФОТО');


                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        alert('статус:' + jqXHR.readyState + "  " + jqXHR.status + "    " + jqXHR.responseText + "   ");
                        alert("Что-то пошло не так. Повторите попытку позже! " + textStatus + " " + errorThrown + " " + jqXHR);
                    }
                });
    });


    $('#quest_image').change(quest_image_change);

    $('#quest_sound').change(sound_change);

    $('input[name=telephone]').change(function ()
    {
        if (checkTelephoneNumber($(this).val()) == false)
        {
            $(this).next().empty();
            $(this).next().css('color', 'red');
            $(this).next().append('Error');

        } else
        {
            $(this).next().empty();
            $(this).next().css('color', 'green');
            $(this).next().append('Ok');
        }
    });

// проверка на правильность вводимого времени уровня
    $('.time_hint').change(function() 
    {
        var t_time= parseInt($(this).val());
        var c_ln= $(this).attr('ln');
        var name=$(this).attr('name');
        name=name.split(']'); 

        if(c_ln>=2)
        {
            if(c_ln==2)
            {
                name=name[0]+'][time_hint_1]';
               var time=parseInt($('input[name="'+name+'"]').val());
               if(time>=t_time)
               {
                    //alert('Время уровня должно')border: 4px double black;
                   $(this).css('border','4px double red' );
                   $(this).val('');
               }
               else
               {
                   $(this).css('border','inherit' );
               }
            }
            else if(c_ln==3)
            {
                name=name[0]+'][time_hint_2]';
               var time=parseInt($('input[name="'+name+'"]').val());
               if(time>=t_time)
               {
                    //alert('Время уровня должно')
                   $(this).css('border','4px double red');
                   $(this).val('');
               }
               else
               {
                   $(this).css('border','inherit' );
               }
            }
        }
    });
    $('input[name=email]').change(function ()
    {
        if (validateEmail($(this).val()) == false)
        {
            $(this).next().empty();
            $(this).next().css('color', 'red');
            $(this).next().append('Error');

        } else
        {
            $(this).next().empty();
            $(this).next().css('color', 'green');
            $(this).next().append('Ok');
        }
    });
    // форма на редактирование  персональных данных пользователя,
    $('form[name=form_user_info]').submit(function ()
    {
        if (checkTelephoneNumber($('input[name=telephone]').val()) == false && $('input[name=telephone]').val()!="")
            return false;
        if (validateEmail($('input[name=email]').val()) == false)
            return false;
        if ($('input[name=new_pass]').val() != "" || $('input[name=confirm_pass]').val() != "")
        {
            if (confirmPassword($('input[name=new_pass]').val(), $('input[name=confirm_pass]').val()) != true)
                return false;
            if (ajaxCheckPassword($('input[name=curr_pass]').val()) == false)
                return false;
        }
        if (validateLogin($('input[name=login]').val()) == false)
        {
            alert('Введите логин!!');
            return false;
        }

        return true;
    });
    // валидация пароля на страничке персональных данных
    $('input[name=curr_pass]').change(function ()
    {
        var password = $(this).val();
        var input = $(this);
        if (ajaxCheckPassword(password) == true)
        {
            input.next().empty();
            input.next().css('color', 'green');
            input.next().append('Ok');
        } else
        {
            input.next().empty();
            input.next().css('color', 'red');
            input.next().append('Error');
        }
    });
    // валидация пароля на страничке персональных данных
    $('input[name=new_pass]').change(function ()
    {
        if (confirmPassword($(this).val(), $('input[name=confirm_pass]').val()) == true) {

            $('input[name=confirm_pass]').next().empty();
            $('input[name=confirm_pass]').next().css('color', 'green');
            $('input[name=confirm_pass]').next().append('Ok');
        } else
        {
            $('input[name=confirm_pass]').next().empty();
            $('input[name=confirm_pass]').next().css('color', 'red');
            $('input[name=confirm_pass]').next().append('Error');
        }
    });
    //валидация пароля на страничке персональных данных
    $('input[name=confirm_pass]').change(function ()
    {
        if (confirmPassword($(this).val(), $('input[name=new_pass]').val()) == true) {
            $(this).next().empty();
            $(this).next().css('color', 'green');
            $(this).next().append('Ok');
        } else
        {
            $(this).next().empty();
            $(this).next().css('color', 'red');
            $(this).next().append('Error')
        }
    });
    $('#buy_button_game_quests').click(function ()
    {
        var button = $(this);
        var idGame = button.attr('id_game');
        var city = button.attr('city');
        var price = button.attr('price');
        var name = button.attr('name');


        $('#myGameBuy').attr('id-invoice', idGame);
        var str = 'Квест: ' + name + '<br> Город: ' + city + ' <br>Стоимость: ' + price + '<br>';
        button = '<button class="btn btn-lg btn-default submit" id="button_submit_form">ОПЛАТИТЬ И СТАТЬ УЧАСТНИКОМ</button>';
        $('#myGameBuy').find('.modal-body').empty();
        $('#myGameBuy').find('.modal-body').prepend(str + button)

    });
    // загрузка главного фото базы по ajax
    $('input[name=main_image]').change(function () {

        var file_data = $(this).prop("files")[0];   // Getting the properties of file from file field
        var form_data = new FormData();                  // Creating object of FormData class
        form_data.append("file", file_data);              // Appending parameter named file with properties of file_field to form_data
        var id = $('input[name=questID]').val();
        form_data.append("id", id);
        $.ajax
                ({
                    url: '/ajax/save/image/',
                    dataType: 'script',
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: form_data, // Setting the data attribute of ajax with file_data
                    type: 'post',
                    success: function (data)
                    {
                        console.log(data);
                        console.log(JSON.parse(data));
                        //$('#img_main').attr('src', JSON.parse(data));
                        $('#img_main').css('background', 'url('+data+') no-repeat center');
                        $('#img_main').css('background-size', 'cover');
                        

                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        alert('статус:' + jqXHR.readyState + "  " + jqXHR.status + "    " + jqXHR.responseText + "   ");
                        alert("Что-то пошло не так. Повторите попытку позже! " + textStatus + " " + errorThrown + " " + jqXHR);
                    }
                });
    });
    $('#myGameBuy').on('click', '#button_submit_form', function () {
        // получим id  и  вставим в форму
        var str = $('#myGameBuy').attr('id-invoice');
        $('input[name=idInvoice]').val(str);
        $('form[name=form_buy_game]').submit();
    });

    $('#close_redirect').click(function ()
    {
        document.location.href = 'personal';
    });
    // отправка ответа по игре !!!!

    // отправка по Ajax подтверждения о получения подсказки
    $('#send_accept_hint').click(sendAcceptHint);
    $('#game_next_level').click(nextLevel);
    $('#show_answer_button').click(show_answer);
    $('#send_answer').click(send_answer);
    $('#stop_game').click(stop_game);

});

function stop_game()
{
    var idGame = $(this).attr('idGame');
    var level = $(this).attr('level');
    $.ajax
            ({
                url: '/game/stop-game/',
                type: 'POST',
                data: {idGame: idGame, level: level},
                dataType: 'json',
                success: function (data)
                {
                    if (data.status == 'success')
                    {
                        document.location.href = 'personal';
                    } else
                    {
                        alert('Упс.Что-то пошло не так :(');
                    }

                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    alert('статус:' + jqXHR.readyState + "  " + jqXHR.status + "    " + jqXHR.responseText + "   ");
                    alert("Что-то пошло не так. Повторите попытку позже! " + textStatus + " " + errorThrown + " " + jqXHR);
                }
            });
}

function send_answer()
{
    var idGame = $(this).attr('idGame');
    var level = $(this).attr('level');
    var answer = $('#answer').val();
    $.ajax
            ({
                url: '/game/check-answer/',
                type: 'POST',
                data: {idGame: idGame, level: level, answer: answer},
                dataType: 'json',
                success: function (data)
                {
                    if (data.status == 'success')
                    {
                        $('#answer_wright').html(answer);
                        $('#answer_wright').parent('div').show();
                        $('#answer_wright').parent('div').parent('div').show();
                        $('#game_panel').hide();
                        $('#game_next_level').show();
                    } else if (data.status == 'end')
                        document.location.href = 'personal';
                    else
                    {
                        alert('ответ не верный');
                        print_send(answer);
                    }

                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    alert('статус:' + jqXHR.readyState + "  " + jqXHR.status + "    " + jqXHR.responseText + "   ");
                    alert("Что-то пошло не так. Повторите попытку позже! " + textStatus + " " + errorThrown + " " + jqXHR);
                }
            });
}


function print_send(answer)
{
    var answers = 5;// количество выводимых ответов
    var current_text = $('#print_send_answers').html();
    var new_answer = answer;
    if (current_text != "") {
        current_text = current_text.split('<br>');

        if (current_text.length < answers)
        {
            current_text.push(new_answer);
        } else
        {
            delete current_text[0];
            current_text.push(new_answer);
        }
        // удалим пустые строки
        var new_array = [];
        for (i = 0; i < current_text.length; i++)
        {
            if (current_text[i] != "" && current_text[i] != undefined)
                new_array.push(current_text[i]);
        }
        $('#print_send_answers').html(new_array.join('<br>'));
        console.log(current_text);
        console.log(current_text.join('<br>'));
    } else
        $('#print_send_answers').html(new_answer + '<br>');

}

// показать ответ
function show_answer()
{
    $("#show_answer .close").click()
    var idGame = $(this).attr('idGame');
    var level = $(this).attr('level');
    $.ajax
            ({
                url: '/game/show-answer/',
                type: 'POST',
                data: {idGame: idGame, level: level},
                dataType: 'json',
                success: function (data)
                {
                    if (data.answer)
                    {
                        $('#answer_wright').html(data.answer);
                        $('#answer_wright').parent('div').show();
                        $('#answer_wright').parent('div').parent('div').show();
                        $('#game_panel').hide();
                        if (!data.next)
                            $('#game_next_level').hide();
                    }

                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    alert('статус:' + jqXHR.readyState + "  " + jqXHR.status + "    " + jqXHR.responseText + "   ");
                    alert("Что-то пошло не так. Повторите попытку позже! " + textStatus + " " + errorThrown + " " + jqXHR);
                }
            });
}

function nextLevel()
{
    var idGame = $(this).attr('idGame');
    var level = $(this).attr('level');
    $.ajax
            ({
                url: '/game/next-level/',
                type: 'POST',
                data: {idGame: idGame, level: level},
                dataType: 'json',
                success: function (date)
                {
                    if (date)
                    {
                        document.location.href = 'game?id=' + idGame;
                    }

                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    alert('статус:' + jqXHR.readyState + "  " + jqXHR.status + "    " + jqXHR.responseText + "   ");
                    alert("Что-то пошло не так. Повторите попытку позже! " + textStatus + " " + errorThrown + " " + jqXHR);
                }
            });
}
// отправка по Ajax подтверждения о получения подсказки, принимает время обратного отсчета до подсказки
function sendAcceptHint()
{
    var idGame = $(this).attr('idGame');
    var idHint = $(this).attr('hintId');
    var level = $(this).attr('level');
    var button = $(this);
    $.ajax
            ({
                url: '/game/accept-hint/',
                type: 'POST',
                data: {idGame: idGame, idHint: idHint, level: level},
                dataType: 'json',
                success: function (date)
                {
                    if (date)
                    {
                        window.location.reload();
//                        window.timeend = new Date(date);
//                        $('#game_panel').show();
//                        button.hide();
                    }

                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    alert('статус:' + jqXHR.readyState + "  " + jqXHR.status + "    " + jqXHR.responseText + "   ");
                    alert("Что-то пошло не так. Повторите попытку позже! " + textStatus + " " + errorThrown + " " + jqXHR);
                }
            });
}
// добавить уровень, на страничке редактирования квеста
function bntClickAddLevel()
{
    var level = $('.count').last().html();
    $.ajax
            ({
                url: 'load/level/form/',
                type: 'POST',
                data: {level: level},
                success: function (data)
                {
                    $('.levels').last().before(data);
                    $('button.btn.plus').hide();
                    $('button.btn.plus').last().show();
                    $('button.btn.plus').last().click(bntClickAddLevel);
                    $('button.btn.minus').last().click(btnClickDeleteLevel);
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    alert('статус:' + jqXHR.readyState + "  " + jqXHR.status + "    " + jqXHR.responseText + "   ");
                    alert("Что-то пошло не так. Повторите попытку позже! " + textStatus + " " + errorThrown + " " + jqXHR);
                }
            });
    return false;
}
//  удлаить уровень, на страничке редактирования квеста
function btnClickDeleteLevel()
{
    $(this).parents('.levels').remove();
    $('button.btn.plus').last().show();
    //console.log($(this).parents('.count').html());
}

function btnClickDeeleQuest()
{
    var id = $(this).attr('id');
    var btn = $(this);
    $.ajax
            ({
                url: '/delete/quest/',
                type: 'POST',
                data: {id: id},
                success: function (data)
                {
                    btn.parents('.row.block').remove();

                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    alert('статус:' + jqXHR.readyState + "  " + jqXHR.status + "    " + jqXHR.responseText + "   ");
                    alert("Что-то пошло не так. Повторите попытку позже! " + textStatus + " " + errorThrown + " " + jqXHR);
                }
            });
}
// таймер  обратного отсчета в игре, до подсказки или ответа
function timer()
{
    var timeend = window.timeend;

    var today = new Date();
    today = Math.floor((timeend - today) / 1000);
    var tsec = today % 60;
    today = Math.floor(today / 60);
    if (tsec < 10 && tsec > -1)
        tsec = '0' + tsec;
    var tmin = today % 60;
    today = Math.floor(today / 60);
    if (tmin < 10 && tmin > -1)
        tmin = '0' + tmin;
    var thour = today % 24;
    today = Math.floor(today / 24);
//var timestr=today +" дней "+ thour+" часов "+tmin+" минут "+tsec+" секунд";
    var timestr = thour + " часов " + tmin + " минут " + tsec + " секунд";
    $('#timer').html(timestr);
    if (today == 0 && thour == 0 && tmin == 0 && tsec <= 0)
        setTimeout(function () {
            window.location.reload();
        }, 3000);
    else
        window.setTimeout("timer()", 1000);
}
// валидация телефоного номера на стрничке персональных данных
function checkTelephoneNumber(str) {
    var exp = /^(\s*)?(\+)?([- _():=+]?\d[- _():=+]?){10,14}(\s*)?$/;
    return exp.test(str);
}
// валидация почты на страничке персональных данных
function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}
function confirmPassword(pass, confirm_pass)
{
    if (pass == confirm_pass && pass.length >= 7 && confirm_pass.length >= 7)
        return true;
    else
        return false;
}
function validateLogin(login)
{
    if (login == undefined || login == "" || login == false)
        return false;
    else
        return true;
}

// проверка пароля по ajax на страничке персональных данных
function ajaxCheckPassword(password)
{
    //var flag = false;
    $.ajax
            ({
                url: '/ajax/check/password/',
                type: 'POST',
                data: {password: password},
                dataType: 'json',
                async : false,
                success: function (data)
                {
                    if (data.status == 'Success')
                    {
                        flag = true;
                    } else
                    {
                        flag = false;
                    }

                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    alert('статус:' + jqXHR.readyState + "  " + jqXHR.status + "    " + jqXHR.responseText + "   ");
                    alert("Что-то пошло не так. Повторите попытку позже! " + textStatus + " " + errorThrown + " " + jqXHR);
                }
            });
    return flag;
}

// после нажатия на кнопку начать игру в квестах
function ajaxCreateInvoice(button)
{
    var idGame = button.attr('id_game');
    var city = button.attr('city');
    var price = button.attr('price');
    var name = button.attr('name');
    $.ajax
            ({
                url: '/add/game/',
                type: 'POST',
                data: {id: idGame},
                dataType: 'json',
                success: function (id)
                {
                    if (id)
                    {
                        $('#myGameBuy').attr('id-invoice', id);
                        var str = 'Квест: ' + name + '<br> Город: ' + city + ' <br>Стоимость: ' + price + '<br>';
                        var button = '<button class="btn btn-lg btn-default submit" id="button_submit_form">ОПЛАТИТЬ И СТАТЬ УЧАСТНИКОМ</button>';
                        $('#myGameBuy').find('.modal-body').empty();
                        $('#myGameBuy').find('.modal-body').prepend(str + button);

                    }

                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    alert('статус:' + jqXHR.readyState + "  " + jqXHR.status + "    " + jqXHR.responseText + "   ");
                    alert("Что-то пошло не так. Повторите попытку позже! " + textStatus + " " + errorThrown + " " + jqXHR);
                }
            });

}
function scrollDown()
{
    //alert(document.location.href);
    var req = document.location.href;
    if (req.indexOf('page=') + 1) {
        var height = $("body").height() - 1100;
        $("html,body").animate({"scrollTop": height}, 1);
    }
}

function save_quest()
{
    if($("form").is("#form_save_quest"))
    {
        var url= $('form[name=form_quest]').attr('action');
        var form= $( 'form[name=form_quest]').serialize();
        $.ajax
                    ({
                        url: url,
                        type: 'POST',
                        data: form,
                        success: function (data)
                        {

                        },
//                        error: function (jqXHR, textStatus, errorThrown)
//                        {
//                            alert('статус:' + jqXHR.readyState + "  " + jqXHR.status + "    " + jqXHR.responseText + "   ");
//                            alert("Что-то пошло не так. Повторите попытку позже! " + textStatus + " " + errorThrown + " " + jqXHR);
//                        }
                    });
    }
}


function delete_sound_file()
{
    var id = $(this).attr('name');
        var btn = $(this);
        $.ajax
                ({
                    url: '/delete/file/',
                    type: 'POST',
                    data: {id: id},
                    success: function (data)
                    {
                        btn.parents('.col-md-7').html('<label class="add_q_file_sound">'+
						'Загрузить'+
    					'<input type="file" name="file_sound" id="quest_sound">'+
    					'<div></div>'+
					'</label>');
                        $('#quest_sound').change(sound_change);        

                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        alert('статус:' + jqXHR.readyState + "  " + jqXHR.status + "    " + jqXHR.responseText + "   ");
                        alert("Что-то пошло не так. Повторите попытку позже! " + textStatus + " " + errorThrown + " " + jqXHR);
                    }
                });
}
function sound_change()
{
    var file_data = $(this).prop("files")[0];
    var name = file_data.name;
    $(this).next().text("");
    $(this).next().text(name);
}

function delete_image_quest()
{
    var id = $(this).attr('name');
        var btn = $(this);
        $.ajax
                ({
                    url: '/delete/image/',
                    type: 'POST',
                    data: {id: id},
                    success: function (data)
                    {
                        btn.parents('.col-md-7').html('<label class="add_q_file_image">'+
						'Загрузить'+
    					'<input type="file" name="file_image" id="quest_image">'+
    					'<div></div>'+
					 '</label>');
                        $('#quest_image').change(quest_image_change);         
                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        alert('статус:' + jqXHR.readyState + "  " + jqXHR.status + "    " + jqXHR.responseText + "   ");
                        alert("Что-то пошло не так. Повторите попытку позже! " + textStatus + " " + errorThrown + " " + jqXHR);
                    }
                });
}

function quest_image_change()
{
     var file_data = $(this).prop("files")[0];
     var name = file_data.name;
     $(this).next().text("");
     $(this).next().text(name);
}