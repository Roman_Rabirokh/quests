<?php
global $user;
$idUser = $user->getID();

$data['fields'] = dbGetRow('SELECT data.*, city.name AS city ,city.id AS cityId, transport.name AS transportName FROM #__content_data_2
AS data
INNER JOIN #__content
AS city ON(data.f5=city.id)
INNER JOIN #__content
AS transport ON (data.f17=transport.id)
WHERE data.id = :dataid' ,array(':dataid'=>$data['id']));
$data['thumb'] = getImageById($data['mainimage'],array('height'=>381, 'width'=>333));
$image=dbGetOne('SELECT id FROM #__images
      WHERE parentid  = :id AND title="big_image"',array(':id' => $data['id']));
$data['big_image']=getImageById($image);
//echo "<pre>";
//print_r($data);
//echo "</pre>";
// если есть и игра, то страничка( кнопка) начинает игру
if(!empty($data['fields']))
    $idGame=!empty($_GET['idGame']) ? $_GET['idGame'] : 0;


if(!empty($idGame))
{
    $dataGame= dbGetOne('SELECT i.id FROM #__invoices AS i '
	    . 'INNER JOIN #__content  AS c ON(c.id=i.quest_id)'
	    . 'WHERE i.status >= 1 AND c.id= :id AND i.id= :idGame', array(':id' => $data['id'], ':idGame' => $idGame));

    if(empty($dataGame))
	$dataGame=0;
}
$action=!empty($_GET['action']) ? $_GET['action'] : 0;

?>

<header class="bg1">
  <?php include 'menu.php'; ?>
<div class="center bg2" <?php if(!empty($data['big_image'])){ ?> style="background: url(http://quests.devtech.com.ua/templates/img/bg/header-3.png) center center, url('<?=$data['big_image']?>') no-repeat center;    background-size: contain, 1100px auto;"  <?php } ?> ></div>
<div class="plashka"><?=$data['name'];?></div>
<div class="bottom none-bg mt-text"></div>
</div>
</header>
<!--<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">ПРИНЯТЬ УЧАСТИЕ</h4>
      </div>
      <div class="modal-body">
        <form action="/create/invoice/" method="post">
        <div class="clearfix"><strong>Имя: </strong><input type="text" class="form-control" placeholder="Введите имя и фамилию" id="name"> <span class="flr">Стоимость: <span class="count">1400</span> руб.</span></div>
        <input type="hidden" name="id" value="<?//=$data['id'];?>">
        <div class="clearfix">
          <strong>Способ оплаты:</strong>
          <div class="dropdown">
            <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
             Выберите удобный способ оплаты</a>
            <span class="icon-caret"></span>
            </button>
            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
              <li><a href="#"> Наличными</a></li>
              <li><a href="#"> Яндекс.Деньги</a></li>
            </ul>
          </div>
        </div>
        <div class="clearfix"><strong>Код сертификата: <span>(при наличии)</span></strong> <input type="text" class="form-control" id="sertif"></div>
        <button class="btn btn-lg btn-default submit">ОПЛАТИТЬ И СТАТЬ УЧАСТНИКОМ</button>
      </div>
    </form>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>-->
<?php include 'modal-buy.php'; ?>
<article class="mt-content">
<div class="container">
<div class="this-q">
  <div class="row information">
    <div class="col-lg-4 col-md-5 col-sm-6 left">
      <div class="icons clearfix">
        <div class="col col-xs-4">
          <div class="time icon-mini">
            <p><i class="icon-wall-clock"></i>Время <strong><?=$data['fields']['f1']?></strong></p>
          </div>
        </div>
        <div class="col col-xs-4">
          <div class="group icon-mini">
            <p><i class="icon-multiple-users-silhouette"></i>Участников <strong><?=$data['fields']['f2']?></strong></p>
          </div>
        </div>
        <div class="col col-xs-4">
          <div class="distance icon-mini">
            <p><i class="icon-speedometer"></i>Растояние <strong><?=$data['fields']['f3']?></strong></p>
          </div>
        </div>
      </div>

	  <div class="img" style="background:url(<?=$data['thumb']?>) no-repeat center;background-size:cover;"></div>
    </div>
    <div class="col-lg-8 col-md-7 col-sm-6 right">
      <div class="row one">
        <div class="col-md-10 col-sm-8 col-xs-8">
          <div class="icon-map"></div>
          <span class="red">Как добраться?</span>
          <span class="address"><?=$data['fields']['f6']?></span>
        </div>
        <div class="col-md-2 col-sm-4 col-xs-4">
          <div class="round-price"><span><strong><?=$data['fields']['f4'];?></strong> руб.</span></div>
        </div>
      </div>
      <div class="row two">
        <div class="col-lg-2 col-md-3 hidden-sm hidden-xs">
          <div class="icon-start-and-finish"></div>
        </div>
        <div class="col-lg-6 col-md-9">
          <div class="row">
            <div class="col-md-6">Начало маршрута:</div>
            <div class="col-md-6"><?=$data['fields']['f7']?></div>
          </div>
          <div class="row lh25">
            <div class="col-md-6">Время прохождения:</div>
            <div class="col-md-6 foz25"> с <?=$data['fields']['f9']?> до <?=$data['fields']['f15']?> </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-12">
          <div class="transport">
            <h4>Разрешенный транспорт:</h4>
            <div class="icons-transport">
		<?php if(strripos($data['fields']['transportName'],'Пешком')!==FALSE){ ?>
              <div class="icon-footsteps-silhouette-variant"></div>
		<?php } else if(strripos($data['fields']['transportName'],'велосипеде')!==FALSE) { ?>
	      <div class="icon-bike-of-a-gymnast"></div>
              <?php } else if(strripos($data['fields']['transportName'],'мотоцикле')!==FALSE) { ?>
	      <div class="icon-bike"></div>
	      <?php } else if(strripos($data['fields']['transportName'],'автомобиле')!==FALSE) { ?>
              <div class="icon-car-of-hatchback-model"></div>
	      <?php }?>
            </div>
          </div>
        </div>
      </div>
      <div class="row three">
        <div class="col-xs-12">
          <h3>г.<?=$data['fields']['city'];?></h3>
          <?php echo htmlspecialchars_decode($data['detail_text']); ?>
        </div>
      </div>
      <div class="row four">
        <div class="col-lg-4 col-md-6">
          <?php if(empty($idUser)) {?>

            <a href="/registration/?quest=<?=$data['fields']['id']?>&price=<?=$data['fields']['f4']?>"><div class="btn btn-lg btn-default red" data-toggle="myGameBuy"
	 >  НАЧАТЬ ИГРАТЬ </div></a>

          <?php } else {?>
	     <?php if(empty($dataGame) && empty($action)){ ?>
            <a href="#" id_game="<?=$data['id'];?>"  city="<?=$data['fields']['city'];?>" price="<?=$data['fields']['f4'];?>" name="<?=$data['name']?>"  id="button_add_game"><div class="btn btn-lg btn-default red" data-toggle="modal" data-target= "#myGameBuy"  >  НАЧАТЬ ИГРАТЬ </div></a>
            <!-- <div class="btn btn-lg btn-default red" data-toggle="modal" data-target= "#myModal"> НАЧАТЬ ИГРАТЬ</div> -->
	     <?php }
             else if(!empty($action)&& $action="show"){

             }
             else { ?>
		<a href='/game/start?id=<?=$dataGame;?>'><div class="btn btn-lg btn-default green" data-toggle="myGameBuy"
	 >  НАЧАТЬ ИГРАТЬ </div></a>
	    <?php } ?>
            <?php } ?>
        </div>

        <div class="col-lg-8 col-md-6">
          <div class="must">
            <div class="title">Для прохождения <br />квеста вам потребуется:</div>
            <div class="perechen">
				<ul>
				<li><?=$data['fields']['f10']?></li>
				</ul>
            </div>
            <!-- <div class="icons-must">
              <div class="icon-photo-camera"></div>
              <div class="icon-thread-ball"></div>
              <div class="icon-tool"></div>
              <div class="icon-flashlight"></div>
            </div> -->
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php
  if(empty($dataGame)){
  $this->includeComponent('quests/similar', array('idQuest' => $data['id'], 'cityId' =>  $data['fields']['cityId']));
  }
//  else
//  {
//
//   $this->includeComponent('users/admin.contacts');
//  }
?>
</div>
</div>
</article>
