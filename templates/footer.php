<script src="/templates/js/main.js?t=<?php echo(microtime(true)); ?>"></script>
<?php $page->getFooter(); ?>

<footer class="f-bg">
	<div class="container">
		<div class="col col-lg-5 col-xs-4 pay"><i class="icon-pay"></i><span class="hidden-md hidden-sm hidden-xs">- МЫ ПРИНИМАЕМ К ОПЛАТЕ</span></div>
		<div class="col col-lg-2 col-xs-4 copy"><span>© 2016 Городские Квесты</span></div>
		<div class="col col-lg-5 col-xs-4 social"><span class="hidden-md hidden-sm hidden-xs">МЫ В СОЦИАЛЬНЫХ СЕТЯХ -</span><a href="" class="icon-insta"></a><a href="" class="icon-vk"></a><a href="" class="icon-fb"></a>
	</div>
</div>
    
    <!-- BEGIN JIVOSITE CODE {literal} -->
<script type='text/javascript'>
(function(){ var widget_id = 'boybXdziz0';var d=document;var w=window;function l(){
var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);}if(d.readyState=='complete'){l();}else{if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})();</script>
<!-- {/literal} END JIVOSITE CODE -->
<?php $this->includeComponent('users/admin.contacts');  ?>
</footer>
</body>
</html>
