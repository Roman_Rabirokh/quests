<?php
global $user;
$idQuest=!empty($_GET['id']) ? ($_GET['id']) : 0 ;  
$isQuest= dbGetOne('SELECT created FROM #__content WHERE id= :id AND userid= :userid', array(':id' =>$idQuest, ':userid'=> $user->getID()));
if(empty($isQuest) && !$user->IsAdmin())
    redirect('/');
if(empty($user->getID()) &&  !$user->IsAdmin())
	redirect('/');
$image=dbGetOne('SELECT id FROM #__images
      WHERE parentid  = :id AND title="big_image"',array(':id' => $idQuest));
$image=getImageById($image);
?>



<header class="bg1">
  <?php include "menu.php"; ?>
    <div class="center bg1" id="big_image_div" <?php if(!empty($image)){ ?> style="background: url(http://quests.devtech.com.ua/templates/img/bg/header-3.png) center center, url('<?=$image?>') no-repeat center top; background-size: contain, 1100px auto;"  <?php } ?> >
			<div class="container">
				<div class="row">
					<div class="col col-xs-12">
						<a class="btn btn-default btn-lg now icon-button-main mt-a-index" id="b_big_image">
						    <?php if(empty($image)){ ?>
						    ВЫБРАТЬ ФОТО ГЛАВНОЙ
						    <?php  } else { ?>
						    ИЗМЕНИТЬ ФОТО
						    <?php } ?>
						</a>
						<input type="file" name="big_image" id="big_image" style="visibility: hidden;">
					</div>
				</div>
			</div>
		</div>
		<div class="plashka">СОЗДАТЬ КВЕСТ</div>
		<div class="bottom none-bg mt-text">
			<div class="container">
				<div class="row">
					<div class="col col-xs-12">
						<p>Текстовая информация которая может содержать любую информацию по сайту или <br>правилам принятия участия в квести, или регистрация лиц и  в таком подобном стиле</p>
					</div>
				</div>
			</div>
		</div>

</header>
    <?php $this->includeComponent('users/quest.form'); ?>
