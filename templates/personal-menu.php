<?php global $app;
global $page;
global $user;
if (!$user->Authorized()) {
    Header("Location:/");
    exit();
    
}
$count= dbGetRow('SELECT COUNT(id) as count FROM #__invoices WHERE user_id= :id OR gift_from_id = :id_from', array(':id'=> $user->getID(), ':id_from'=> $user->getID()));
$count=$count['count'];
$countCreateQuests=dbGetRow('SELECT COUNT(id) AS count FROM #__content WHERE userid= :userid', array(':userid' => $user->getID()));
$countCreateQuests=$countCreateQuests['count'];
?>


	<div class="container">
	    <div class="row navigation">
			<div class="col col-xs-12 zero">
				<a class="btn btn-default btn-lg icon-button" href="/personal/info/">ЛИЧНЫЕ ДАННЫЕ</a>
				<a class="btn btn-default btn-lg icon-button" href="/personal/">Мои квесты(<span class="count"><?php echo $count?></span>)</a>
				<a class="btn btn-default btn-lg icon-button" href="/personal/created-quests">Создание квестов(<span class="count"><?php echo $countCreateQuests?></span>)</a>
			</div>
		</div>
		<br/>
		<br/>
		<br/>
	</div>
   
