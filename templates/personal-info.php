<?php
if (!defined("_APP_START")) {
    exit();
}

global $app;
global $page;
global $user;
$app->actionResult = TRUE;
if (!$user->Authorized()) {
    Header("Location:/");
    exit();
}
$count = dbGetOne('SELECT count(id) FROM #__invoices WHERE user_id=:id', array(':id' => $user->getID())) - 1;
?>

<header class="bg2">
    <?php include 'menu.php'; ?>

    <div class="center">
	<div class="container">
	    <div class="row">
		<div class="col col-xs-12">
		    <h2>ПРОВЕРЬ СЕБЯ НА СМЕЛОСТЬ</h2>
		    <h1>ЛУЧШИЕ ГОРОДСКИЕ КВЕСТЫ <br/>У ТЕБЯ В ГОРОДЕ</h1>
		    <a href="/add/quest/" class="btn btn-default btn-lg now icon-button-main" id="start_q_btn">СОЗДАЙ СВОЙ КВЕСТ</a>
		</div>
	    </div>
	</div>
    </div>
    <div class="bottom">
	<div class="container">
	    <div class="row">
		<div class="col col-xs-12">
		    <p>Персональные данные</p>
		</div>
	    </div>
	</div>
    </div>
</div>
</header>
<br/>
<br/>
<br/>
<br/>
<br/>
<article class="mt-content">
    <?php
    include 'personal-menu.php';
    $app->includeComponent('users/form.info');
    ?>
</article>  