<?php
global $user;
// редачить страничку от админа!!!
if($user->IsAdmin())
{
    $id=!empty($_GET['id']) ?  $_GET['id'] : 0;
    if(!empty($id))
    {
	$user->AuthorizeByID($id);
    }
}

$idUser = $user->getID();

// Пагинация
$pages = isset($_GET['page']) ? $_GET['page'] - 1 : 0; // страница
$limit = 3; // количество отображаеммых квестов  на одной странице
$start = $limit * $pages;
$countPages = 0;




$data['ITEMS'] = dbQuery("SELECT c.name, c.detail_text, c.mainimage, c.url, c.id, c.active,
  cdata.*, city.name AS city, games.id AS idGame, games.status AS status,  games.rating, games.secret_code AS code
  FROM #__invoices
  AS games
  INNER JOIN #__users
  AS u ON(u.id  = games.user_id OR u.id= games.gift_from_id)
  INNER JOIN #__content
  AS c ON (c.id = games.quest_id)
  INNER JOIN #__content_data_2
  AS cdata ON(c.id  = cdata.id)
  LEFT OUTER JOIN #__content
  AS city ON(cdata.f5 =  city.id)
  WHERE c.content_type = :cType AND u.id  = :id
  ORDER BY games.date_create DESC
  LIMIT :start , :end",
	//LIMIT $offset,1",
	array(':cType' => 2, 'id' => $idUser, ':start' => $start, ':end' => $limit));

 $countPages = dbGetOne('SELECT  COUNT(games.id)
  FROM #__invoices
  AS games
  INNER JOIN #__users
  AS u ON(u.id  = games.user_id OR u.id= games.gift_from_id)
  INNER JOIN #__content
  AS c ON (c.id = games.quest_id)
  WHERE c.content_type = :cType AND u.id  = :id
  ORDER BY games.date_create DESC ', array(':cType' => 2, 'id' => $idUser));

foreach ($data['ITEMS'] as &$item) {
    $item['thumb'] = getImageById($item['mainimage'], array('height' => 381, 'width' => 333));
    $item['url'] = Content::contentUrl($item['url']);
    if ($item['status'] == 3) // игра закончена
	$item['time_game'] = getTimeGame($item['idGame']);
   $item['detail_text']=mb_substr($item['detail_text'],0,100).'...';
}



unset($item);
// Пагинация, выборка отоборжаемых номеров страниц
$countViewPages = 5; // количество отображаемых страниц

$pages++; // прибавляем 1 для отображения.
if ($countPages > $limit)
    $countPages = ceil($countPages / $limit);
else
    $countPages = 1;



if (($pages + 2) >= $countPages) {
    $endPage = $countPages;
} else {
    if (($pages + 2) >= $countViewPages)
	$endPage = $pages + 2;
    else
	$endPage = $countViewPages;
}
if (($pages - 2) < 2) {
    $startPage = 1;
} else {
    $startPage = $pages - 2;
    if (($endPage - $startPage) < $countViewPages && (($pages - 2) > 2)) {
	$startPage = $endPage - 4;
    }
}
?>

<?php foreach ($data['ITEMS'] as $key => $item) { ?>

    <!-- START NEW GAME MODAL FORM -->
    <div id="myGameStart-<?= $item['id'] ?>" class="modal fade">
        <div class="modal-dialog">
    	<div class="modal-content">
    	    <div class="modal-header">
    		<button class="close" type="button" data-dismiss="modal">×</button>
    		<h4 class="modal-title">Старт квеста</h4>
    	    </div>
    	    <div class="modal-body">
    		<!-- FORM BODY NEW GAME START -->
    		Квест: <?= $item['name'] ?> <br>
    		Город: <?= $item['city'] ?> <br>
    		<form role="form" method="post" action="/game/check-code/">
    		    <div class="form-group">
    			<label for="secret_code">Секретный код</label>
    			<input type="text" class="form-control" name="secret_code" id="secret_code" placeholder="Введите секретный код">
    		    </div>
    		    <div class="form-group">
    			<input type="text" name="qid" value="<?= $item['id'] ?>" hidden="hidden">
    		    </div>
    		    <button type="submit" class="btn btn-default btn-success btn-block"><span class="glyphicon glyphicon-off"></span> Играть</button>
    		</form>
    		<!-- FORM BODY END -->
    	    </div>
    	    <div class="modal-footer">
    		<button class="btn btn-default" type="button" data-dismiss="modal">
    		    Закрыть
    		</button>
    	    </div>
    	</div>
        </div>
    </div>


<?php } ?>
<?php include 'modal-buy.php'; ?>
<header class="bg2">
    <?php include 'menu.php'; ?>

    <div class="center">
	<div class="container">
	    <div class="row">
		<div class="col col-xs-12">
		    <h2>ПРОВЕРЬ СЕБЯ НА СМЕЛОСТЬ</h2>
		    <h1>ЛУЧШИЕ ГОРОДСКИЕ КВЕСТЫ <br/>У ТЕБЯ В ГОРОДЕ</h1>
		    <a  href="/add/quest/" class="btn btn-default btn-lg now icon-button-main" id="start_q_btn">СОЗДАЙ СВОЙ КВЕСТ</a>
		</div>
	    </div>
	</div>
    </div>
    <div class="bottom">
	<div class="container">
	    <div class="row">
		<div class="col col-xs-12">
		    <h4>МОИ КВЕСТЫ</h4>
		</div>
	    </div>
	</div>
    </div>
</div>
</header>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>


    <article class="mt-content myquests-list">
        <div class="container">
	    <?php include 'personal-menu.php'; ?>
    	<br/>
    	<br/>
    	<br/>
    	<div class="list-q">


	    <form class="check-code" action="/game/check-code/" method="POST" style="color:black;" >
		<span style="color:white">Введите код</span> <input class="form-control" type="text" name="code">
		<input type="submit">
	    </form>
	    <br>
		<hr>
	    <br>
	    <?php foreach ($data['ITEMS'] as $key => $item) {
		?>
    	    <div class="row block" id="<?= $item['idGame']; ?>">
    		<div class="col-md-3 left">
    		    <div class="img" style="background:url(/templates/img/icons/border/big.png) no-repeat center,url(<?= $item['thumb'] ?>) no-repeat center;background-size: 100%,cover;"></div>
    		    <div class="round-price"><span><strong><?= $item['f4']; ?></strong> руб.</span></div>
    		</div>
    		<div class="col-md-9 right">
    		    <div class="row one">
			<div class="col-lg-7 col-md-7">
			    <h4><?=$item['name'];?></h4>

    			    <h3>г.<?= $item['city']; ?></h3>
				<?= htmlspecialchars_decode($item['detail_text']); ?>
    			</div>
    			<div class="col-lg-5 col-md-5 text-right block-button-myquests-list">
				<?php
				// sets different link paths for paid and unpaid quests
				if ($item['status'] == 0) {
				    $link = "/game/buy?id={$item['idGame']}";
				    //$formID = '#myGameBuy-'.$item['id'];
				} else if ($item['status'] == 1) {
				    $link = "/game/start?id={$item['idGame']}";
				    //$formID = '#myGameStart-'.$item['id'];
				}
				// *** end ***
				?>

    			    <!-- START/BUY game button -->
				<?php
				if ($item['status'] == 0) {
				    echo ('<div class="text_wborder">Цена: ' . $item['f4'].'</div>');
				} else {
				    echo ('<div class="text_wborder">Оплачено: ' . $item['f4'].'</div>');
				}
				?>
    			    <div>
				    <?php if (empty($item['code'])) { ?>
					<?php if ($item['status'] == 0) { ?>
	    				<button class="btn btn-md btn-default edit " data-toggle="modal" data-target="#myGameBuy" id_game="<?= $item['idGame']; ?>" city="<?= $item['city']; ?>" price="<?= $item['f4']; ?>" name="<?= $item['name'] ?>" id="buy_button_game_quests"> Купить               </button>
					<?php } else if ($item['status'] == 1) {
                         if($item['active']==0 || $item['active']==2 || $item['active']==3 )
                         {?>

                        <div class="text_wborder">Квест на модерации, обращайтесь в поддержку</div>
                         <?}
                         else {
                        ?>
	    				<a href='<?=$item['url']?>?idGame=<?= $item['idGame']; ?>'> <button class="btn btn-md btn-default edit" qid="<?= $item['id'] ?>" city="<?= $item['city']; ?>" price="<?= $item['f4']; ?>" name="<?= $item['name'] ?>"> Играть               </button> </a>

                         <?php
                         }

                         } else if ($item['status'] == 3) { ?>
	    				<div class="text_wborder"> <span class="green_text">Пройден</span>
	    				Время: <span class="green_text"> <?= $item['time_game'] ?> </span> минут</div>
                        <?php if(!$item['rating']) { ?>

                       <div class="text_wborder rating_form_blck"><span> Оцените квест</span>
                        <form action="/game/add/rating/" method="POST" name="add-rating">
                            <input type="hidden" name="id-invoice" value="<?=$item['idGame'];?>" >
                            <input type="number" class="form-control" size="3" min="1" max="10" name="rating">
                            <input class="btn btn-md btn-default edit" type="submit" value="Оценить">
                        </form>
						</span>
                        <?php  } else { ?>
                        <div class="text_wborder">Вы поставили квесту : <?php if($item['rating'] <= '3') {echo '<span style="color:red;">'.$item['rating'].'</span>';} elseif ($item['rating'] >='4' & $item['rating'] <= '6') {echo '<span style="color:orange;">'.$item['rating'].'</span>';} else {echo '<span class="green_text">'.$item['rating'].'</span>';};?></div>
                        <?php  }?>
                    <?php } else if ($item['status'] == 2) {
                        if($item['active']==0 || $item['active']==2 || $item['active']==3 )
                         {?>

                        <div class="text_wborder">Квест на модерации, обращайтесь в поддержку</div>
                         <?}
                         else {
                        ?>
	    				<a href='/game/start?id=<?= $item['idGame']; ?>'> <button class="btn btn-md btn-default edit" qid="<?= $item['id'] ?>" city="<?= $item['city']; ?>" price="<?= $item['f4']; ?>" name="<?= $item['name'] ?>"> Продолжить игру               </button> </a>
					<?php
                         }

                    }
				    }
				    ?>


    				<!-- Sertificate present code -->
    <?php if ($item['status'] == 1) { ?>
					<div>
					    <div id="gift_code" style=<?php if (empty($item['code'])) { ?>"display:none" <?php } ?>><div class="text_wborder">Код: <span class="red_text"><?= $item['code']; ?></span> Передайте игроку</div></div>
	<?php if (empty($item['code'])) { ?>
	    				    <button class="btn btn-md btn-default edit send_gift" gid="<?= $item['idGame'] ?>">
	    					Подарить Игру
	    				    </button>
					<?php } ?>
					</div>
				    <?php } ?>
    				<!-- DELETE from invoice game button -->
    <?php if ($item['status'] == 0) { ?>
					<button divid="<?= $item['idGame']; ?>" class="btn btn-md btn-default edit remove" style="background-color:red">Удалить</button> <? } ?>
    			    </div>
    			</div>
				  </div>
    			<div class="row two">
    			    <div class="col-lg-6">
    				<div class="icons clearfix">
    				    <div class="col col-md-4">
    					<div class="time icon-mini">
    					    <p><i class="icon-wall-clock"></i>Время <strong><?= $item['f1']; ?></strong></p>
    					</div>
    				    </div>
    				    <div class="col col-md-4">
    					<div class="group icon-mini">
    					    <p><i class="icon-multiple-users-silhouette"></i>Участников <strong><?= $item['f2']; ?></strong></p>
    					</div>
    				    </div>
    				    <div class="col col-md-4">
    					<div class="distance icon-mini">
    					    <p><i class="icon-speedometer"></i>Растояние <strong><?= $item['f3']; ?></strong></p>
    					</div>
    				    </div>
    				</div>
    				<div class="other-info">
    				    <span>Тип игры: <strong>Командный</strong></span>
    				    <span>Зарегистрировано участников:  <strong>5</strong> из <strong>10</strong></span>
    				</div>
    			    </div>
    			    <div class="col-lg-6">
    				<div class="marshrut">
    				    <div class="row">
    					<div class="col-md-2"><div class="icon-start-and-finish"></div></div>
    					<div class="col-md-10">
    					    <div class="row">
    						<div class="col-md-5">Начало маршрута:</div>
    						<div class="col-md-7"><?= $item['f7'] ?></div>
    					    </div>
    					</div>
    				    </div>
    				</div>
    			    </div>
    			</div>

    		</div>
    	    </div>
	    <?php } ?>
             <?php if($endPage>1) { ?>
  <div class="col col-xs-12" id="pagination">
    <ul class="pagination">

      <li <?php if($pages==$startPage) {?> class="disable" <? } ?> > <a href="<? if($pages!=$startPage){ $tmp=$pages-1; echo ("?page=".$tmp); } else echo "#";?>" class="icon-caret-prev"> </a></li>
      <?php for($i=$startPage; $i<=$endPage; $i++)
      { ?>
	  <li <?php if($i==$pages) {?>  class="disable"  <? }?> ><a href="?page=<?echo($i)?>"><?=$i ?></a></li>

   <?php  } ?>
	  <li <?php if($pages==$endPage) {?> class="disable" <? } ?> ><a href="<?php $tmp=$pages+1; if($pages!=$endPage) { echo ("?page=".$tmp); } else echo "#"; ?>" class="icon-caret-next"> </a></li>
    </ul>
  </div>
 <?php } ?>

    	</div>
    </article>
