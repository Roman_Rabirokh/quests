<?php
$data['login']= dbGetOne('SELECT IF(login!="", login, email) FROM #__users WHERE id= :id', array(':id' => $user->getID()));
?>

<div class="top">
  <div class="container">
    <div class="row">
      <div class="navbar navbar-static-top" role="navigation">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand"  href="http://<?php echo $_SERVER["SERVER_NAME"];?>"><img src="/templates/img/logo.png" alt=""><span>Городские <br>Квесты</span></a>
        </div>
        <ul class="nav navbar-nav navbar-right">
          <?php if($user->Authorized() === FALSE){?>
            <li><a href="/login/" class="btn btn-default sign-in icon-mini"><i class="icon-male"></i>ВОЙТИ</a></li>
          <?php }else{ ?>
	    <?=$data['login']?>
	    <br>
	    <br>
            <li><a href="/logout/" class="btn btn-default sign-in icon-mini"><i class="icon-male"></i>ВЫЙТИ</a></li>
          <?php } ?>
        </ul>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-left">
            <?php if($user->Authorized()){?>
              <li class="active"><a href="http://<?=_SITE?>/personal/"><b>Личный кабинет</b></a></li>
            <?php } ?>
            <li class="active"><a href="http://<?=_SITE?>/">КВЕСТЫ</a></li>
            <li><a href="http://<?=_SITE?>/rules/">Правила</a></li>
            <li><a href="http://<?=_SITE?>/certificates/">Сертификаты</a></li>
            <li><a href="http://<?=_SITE?>/contacts/">Контакты</a></li>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>
</div>
