<html>
<head>
<?php $page->getHeader(); ?>
<!DOCTYPE html>
<html lang="ru">
	<head>
		<meta charset="UTF-8">
		<title>Титлус</title>
		<meta name="keywords" content="" />
		<meta name="description" content="" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
		<link rel="icon" type="image/ico" href="/templates/favicon.ico" />
		<link rel="icon" type="image/png" href="/templates/img/favicon/16x16.png" sizes="16x16" />
		<link rel="icon" type="image/png" href="/templates/img/favicon/32x32.png" sizes="32x32" />
		<link rel="icon" type="image/png" href="/templates/img/favicon/96x96.png" sizes="96x96" />
		<link rel="apple-touch-icon" href="/templates/img/favicon/57x57.png" sizes="57x57" />
		<link rel="apple-touch-icon" href="/templates/img/favicon/60x60.png" sizes="60x60" />
		<link rel="apple-touch-icon" href="/templates/img/favicon/72x72.png" sizes="72x72" />
		<link rel="apple-touch-icon" href="/templates/img/favicon/76x76.png" sizes="76x76" />
		<link rel="apple-touch-icon" href="/templates/img/favicon/114x114.png" sizes="114x114" />
		<link rel="apple-touch-icon" href="/templates/img/favicon/120x120.png" sizes="120x120" />
		<link rel="apple-touch-icon" href="/templates/img/favicon/144x144.png" sizes="144x144" />
		<link rel="apple-touch-icon" href="/templates/img/favicon/152x152.png" sizes="152x152" />
		<meta name="msapplication-TileImage" content="/templates/img/favicon/144x144.png">
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
		<link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,900" rel="stylesheet">
		<link rel="stylesheet" href="/templates/css/libs.min.css">
		<link rel="stylesheet" href="/templates/css/global.css?v=<?php echo uniqid(); ?>">
		<script src="/templates/js/libs.min.js"></script>
		<script src="/templates/js/global.js"></script>
<!--                <script src="/templates/libs/bootstrap/dist/js/bootstrap.min.js"></script>-->

		<!-- datepicker -->
		<link rel="stylesheet" href="/templates/libs/jquery-ui/jquery-ui.min.css">
		<link rel="stylesheet" href="/templates/libs/jquery-ui/jquery-ui.theme.min.css">
		<script src="/templates/libs/jquery-ui/external/jquery/jquery.js"></script>
		<script src="/templates/libs/jquery-ui/jquery-ui.min.js"></script>
		<script src="/templates/libs/jquery-ui/datepicker-ru.js"></script>
		<!-- datepicker -->
</head>
<body class="<?php echo $page->getBodyClass(); ?>">
