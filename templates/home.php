<?php
// this code clear session for previously choosen quest before registration/authorization
if(!empty($_SESSION['quest'])){
  unset($_SESSION['quest']);
  unset($_SESSION['price']);
}

?>

<header class="bg2">
  <?php include 'menu.php'; ?>

  <div class="center">
    <div class="container">
      <div class="row">
        <div class="col col-xs-12">
          <h2>ПРОВЕРЬ СЕБЯ НА СМЕЛОСТЬ</h2>
          <h1>ЛУЧШИЕ ГОРОДСКИЕ КВЕСТЫ <br/>У ТЕБЯ В ГОРОДЕ</h1>
          <a href="/add/quest/" class="btn btn-default btn-lg now icon-button-main"id="start_q_btn">СОЗДАЙ СВОЙ КВЕСТ</a>
        </div>
      </div>
    </div>
  </div>
  <div class="bottom">
    <div class="container">
      <div class="row">
        <div class="col col-xs-12">
          <p>Текстовая информация которая может содержать любую информацию по сайту или <br />правилам принятия участия в квести, или регистрация лиц и  в таком подобном стиле</p>
        </div>
      </div>
    </div>
  </div>
</div>
</header>

<!-- Основное модальное окно! скрыл пока не надо смотреть клиенту -->
<!--<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">ПРИНЯТЬ УЧАСТИЕ</h4>
      </div>
      <div class="modal-body">
        <div class="clearfix"><strong>Имя: </strong><input type="text" class="form-control" placeholder="Введите имя и фамилию" id="name"> <span class="flr">Стоимость: <span class="count">1400</span> руб.</span></div>
        <div class="clearfix">
          <strong>Способ оплаты:</strong>
          <div class="dropdown">
            <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
             Выберите удобный способ оплаты</a>
            <span class="icon-caret"></span>
            </button>
            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
              <li><a href="#"> Наличными</a></li>
              <li><a href="#"> Яндекс.Деньги</a></li>
            </ul>
          </div>
        </div>
        <div class="clearfix"><strong>Код сертификата: <span>(при наличии)</span></strong> <input type="text" class="form-control" id="sertif"></div>
        <button class="btn btn-lg btn-default submit">ОПЛАТИТЬ И СТАТЬ УЧАСТНИКОМ</button>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>-->

<article>
<div class="container">
<?php
$params = array();
if($data['content_type'] == 3)
{
  $params['cityId'] = $data['id'];
}
else if($data['content_type'] == 5)
{
  $params['questType'] = $data['id'];
}
$params['page'] = !empty($_GET['page']) ?  $_GET['page'] : 1;
$params['transport']= !empty($_GET['transport']) ? $_GET['transport'] : 0;
$this->includeComponent('quests/filter',$params); ?>
<div class="row list">
<? $this->includeComponent('quests/list',$params); ?>
</div>

<div id="description" class="row">
  <div class="col-xs-12">
  <?php echo htmlspecialchars_decode($data['preview_text']); ?>
  </div>
</div>
</div>
</article>
