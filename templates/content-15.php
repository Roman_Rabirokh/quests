<header class="bg1">
  <?php include 'menu.php'; ?>
		<<div class="center bg1"></div>
		<div class="plashka">КОНТАКТЫ</div>
		
</header>


<article class="mt-content mt-content-rules">
	<div class="container">
		<ul class="breadcrumbs m40">
			<li><a href="/">Главная</a></li>
			<li>Контакты</li>
		</ul>
		<div class="content m40">
			
			<h3>Наш адрес</h3>
			<p class="contact_address">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
			<h4>Наш телефон</h4>
			<p class="contact_address">+123 456 789</p>
			<h4>Или свяжитесь с нами одним из удобных для вас способом:</h4>
			<p class="contact_social">
			     <ul class="contact_social_list text-center">
			         <li><a href="#"><img src="/templates/img/icons/viber.png" alt="">Viber</a></li>
			         <li><a href="#"><img src="/templates/img/icons/whatsapp.png" alt="">Whatsapp</a></li>
			         <li><a href="#"><img src="/templates/img/icons/skype.png" alt="">Skype</a></li>
			         <li><a href="#"><img src="/templates/img/icons/email.png" alt="">email@mail.ru</a></li>
			    
			     </ul>
			</p>
			<p><iframe src="https://api-maps.yandex.ru/frame/v1/-/C6UXEO9w" width="100%" height="400" frameborder="0"></iframe></p>
		</div>
	</div>
</div>
</article>