<?php 

$files= dbQuery('SELECT * FROM #__files WHERE source = 1 AND parentid = :parentid',array(":parentid"=>$_GET["id"]));
?>
<div class="panel panel-default">
	<div class="panel-body">
		<div id="filesUpload">
			<div class="form-inline">
				<input type="file" multiple name="files[0][file]" class="form-control" >
				<input type="text" multiple name="files[0][title]" placeholder="Наименование" class="form-control" >
				<input type="text" multiple name="files[0][description]" placeholder="Описание" class="form-control" >
				<input type="text" multiple name="files[0][showorder]" placeholder="Порядок" class="form-control" >
			</div><br />
		</div>
		<div class="form-inline">
			<input type="button" onclick="addFile()" value="Добавить файл" class="btn btn-default" />
		</div>	
	</div>
</div>
<?php if(!empty($files)) {?>
<div class="panel panel-default">
	<div class="panel-body">
		<table class="table">
			<?php foreach($files as $file) { ?>
			<tr>
				<td><input type="checkbox" /></td>
				<td><a target="_blank" href="<?php echo _FILES_URL; ?><?php echo $file["filename"]; ?>" ><?php echo $file["originalname"]; ?></a></td>
				<td><?php echo $file["filesize"]; ?></td>
				<td><input type="text" value="<?php echo $file["title"]; ?>" placeholder="Наименование" class="form-control" /></td>
				<td><input type="text" value="<?php echo $file["description"]; ?>" placeholder="Описание" class="form-control" /></td>
					<td><input type="text" value="<?php echo $file["showorder"]; ?>" placeholder="Порядок" class="form-control" /></td>
				<td><a href="javascript:void(0);" onclick="rmFile(<?php echo $file["id"]; ?>)" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span></a></td>
			</tr>
			<?php } ?>
		</table>
	</div>
</div>	
<?php } ?>