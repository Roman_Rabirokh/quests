<?php

$title = 'Пользователи';

$table = '#__users';

$source = 'SELECT u.id,u.name,u.surname,u.email,u.percent,'
	. '(SELECT sum(price) FROM #__invoices WHERE status >= 1 AND user_id = u.id) as price,'
	. '(SELECT COUNT(id) FROM #__content WHERE userid = u.id) AS quests,'
	. 'u.id as price_created, '
	. 'u.id  as sum_percent, '
	. 'u.id  as personal '
	. 'FROM  ' . $table . ' u';



$sort_changes['price'] = 'price';
$sort_changes['quests'] = 'quests';

$title_fields["surname"] = "Фамилия";
$title_fields["name"] = "Имя";
$title_fields["email"] = "E-mail";
$title_fields["price"] = "Купленно игр на сумму";
$title_fields["price_created"] = "Купленно созданных игр на сумму";
$title_fields["quests"] = "Cоздано квестов";
$title_fields["percent"] = "Процент";
$title_fields["sum_percent"] = "Cуммы выплат по процентам";
$title_fields["personal"] = "Войти в личный кабинет";

$edit_title_fields["name"] = "Имя";
$edit_title_fields["email"] = "E-mail";
$edit_title_fields["passw"] = "Пароль";
$edit_title_fields["surname"] = "Фамилия";
$edit_title_fields["created"] = "Создан";
$edit_title_fields["active"] = "Активность";
$edit_title_fields["blocked"] = "Заблокирован";
$edit_title_fields["telephone"] = "Телефон";
$edit_title_fields["city"] = "Город";
$edit_title_fields["last_login"] = "Дата последнего входа";
$edit_title_fields["last_ip"] = "IP последнего входа";
$edit_title_fields["login"] = "Логин";
$edit_title_fields["percent"] = "Процент вознаграждения";


$controls["city"] = new Control("city","list","Город",getListCity());

$controls['name'] = new Control('name','text','Имя');
$controls['name']->required = FALSE;

$eval_fields['price_created'] = "showPriceCreated(\$row);";

$eval_fields['sum_percent'] = "showSumPercent(\$row);";

$eval_fields["personal"] = "showPersonalLink(\$row);";


$unsorted_fields[] = 'price_created';

$unsorted_fields[] = 'sum_percent';

$unsorted_fields[] = 'personal';


function showPriceCreated($row)
{
    $sum= dbGetOne('SELECT SUM(i.price) FROM #__invoices AS i '
	    . 'INNER JOIN #__content AS c ON(c.id=i.quest_id)'
	    . 'INNER JOIN #__users AS u ON(u.id=c.userid)'
	    . 'WHERE u.id= :id', array(':id' => $row['id']));
    if(empty($sum))
	$sum=0;
    print_r ($sum);
}

function showSumPercent($row)
{
    $sum= dbGetOne('SELECT SUM(i.sum_paid_user) FROM #__invoices AS i '
	    . 'INNER JOIN #__content AS c ON(c.id=i.quest_id)'
	    . 'INNER JOIN #__users AS u ON(u.id=c.userid)'
	    . 'WHERE u.id= :id', array(':id' => $row['id']));
    if(empty($sum))
	$sum=0;
    print_r ($sum);
}

function getListCity()
{
     $cities= dbQuery('SELECT name, id FROM #__content WHERE content_type = 3');
     $new_cities=array();
     foreach($cities as $city)
     {
	 $new_cities[$city['id']]=$city['name'];
     }
     return $new_cities;
}

function showPersonalLink($row)
{
    ?><a href="/personal?id=<?php echo $row["id"]; ?>">Редактировать : <?php echo $row['name'] ?>  </a> <?php
}