<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
if(isAjax())
{
	if(!empty($_REQUEST["action"]))
	{
		switch($_REQUEST["action"])
		{
			case "set_active_quest":
					dbNonQuery("UPDATE #__content SET active = :status WHERE id = :id",array(":id"=>$_REQUEST["id"], ':status' =>(int)$_REQUEST["state"]));

				break;
		}
	}
}


$tabs[0]->addField('rating');

$title_fields["rating"] = "Рейтинг";
$edit_title_fields["rating"] = "Рейтинг";
$sort_changes['rating'] = 'rating';
$source="SELECT id,active,mainimage,name,showorder,rating FROM rs_content WHERE 1=1 AND (parentid = '0' OR parentid = '-1') AND content_type = '2'";

//$eval_fields["rating"] = "show_quest_edit(\$row);";



$title_fields["active"] = "Статус";
$eval_fields["active"] = "show_active_quest(\$row);";

//$title_fields["showorder"] = "Редактировать";
$eval_fields["name"] = "show_quest_edit(\$row);";
$scripts[] = "config/js/content-type-2.js";


function show_active_quest($row)
{?>
		<select class="select_active" data-id="<?php echo $row["id"]; ?>">
		    <option class="option active" value="0" <?php  if($row["active"]=='0') { ?> selected <?php } ?> >Cоздан</option>
		    <option class="option active" value="1" <?php  if($row["active"]=='1') { ?> selected <?php } ?> >Опубликован</option>
		    <option class="option active" value="2"<?php  if($row["active"]=='2') { ?> selected <?php } ?> >Отправлен на модерацию</option>
		    <option class="option active" value="3"<?php if($row["active"]=='3') { ?> selected <?php } ?> >Отклонен</option>
		</select> <?php
    
}
function show_quest_edit($row)
{
    ?><a href="/edit/quest?id=<?php echo $row["id"]; ?>">Редактировать : <?php echo $row['name'] ?>  </a> <?php
}