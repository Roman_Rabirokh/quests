<?php

$title = 'Формы';

$table = '#__forms';


$source = "SELECT f.id, fl.name,DATE_FORMAT(f.created,'%d.%m.%Y<br />%H:%i') as fcreated,f.ip,f.id as fields FROM #__forms f
INNER JOIN #__forms_list fl ON fl.id = f.formid
";

$title_fields['fcreated'] = 'Дата';
$title_fields['ip'] = 'IP';
$title_fields['fields'] = 'Данные формы';

$sort_changes['fcreated'] = 'created';

$unsorted_fields = array('fields');

$eval_fields['fields'] = "formdata(\$row);";

function formdata($row)
{
  $data = dbQuery('SELECT ff.name,f.value FROM #__forms_data f
    INNER JOIN #__forms_fields ff ON ff.id = f.fieldid
      WHERE f.formid = :id
      ORDER BY ff.showorder
  ',array(':id'=>$row['id']));
  if(!empty($data))
  {
    ?>
    <table>
        <?php foreach($data as $current) {?>
          <tr>
            <td><?php echo $current['name']; ?>:&nbsp;</td>
            <td><strong><?php echo $current['value']; ?></strong></td>
          </tr>
        <?php } ?>
    </table>
    <?php
  }
}

function after_delete($id)
{
  dbNonQuery('DELETE FROM #__forms_data WHERE formid = :id',array(':id'=>$id));
}
