<?php

if(isset($_GET["logout"]))
{
	session_start();
	session_destroy();
	header("Location: login.php");
	exit();
}
else
{

session_start();
include_once("function/init.php");
function check_user(&$currentuser)
{
	$count = dbGetOne("SELECT id FROM #__direct_users WHERE login = :login AND passw = :passw",array(
		":login"=>$_POST["login"],
		":passw"=>md5($_POST["passw"]),
	));
	if($count > 0)
	{
		$user = dbGetRow("SELECT * FROM #__direct_users WHERE login = :login AND passw = :passw",array(
		":login"=>$_POST["login"],
		":passw"=>md5($_POST["passw"]),
	));
		$currentuser->ID = $user["id"];
		$currentuser->Name = $user["name"];
		$currentuser->Group = $user["groupid"];
		$currentuser->Menu = $user["menuid"];

		dbNonQuery("UPDATE #__direct_users SET last_login = now(),last_ip = '$_SERVER[REMOTE_ADDR]' WHERE id = :id",array(":id"=>$user["id"]));
		global $db;
		closeConnection($db);
		return true;
	}
	else
	{
		return false;
	}
}
if(isset($_GET["login"]) && isset($_POST["db"]) )
{
	
	if(checkToken("login",$_POST["token"]))
	{
		$success = false;
		$db = intval($_POST["db"]);
	
		if(isset($db_array[$db]))
		{
			
			$user = new User($db_array[$_POST["db"]][3],$db_array[$_POST["db"]][4],$db_array[$_POST["db"]][1],$db_array[$_POST["db"]][0],$db_array[$_POST["db"]][2]);
				
			initConnectionParams(array(
				"_HOST"=>$db_array[$db][0],
				"_DB"=>$db_array[$db][1],
				"_DBUSER"=>$db_array[$db][3],
				"_DBPASSWORD"=>$db_array[$db][4]
			));
		
			if(check_user($user))
			{
				$success = true;
				
			}
			else
			{
				$_SESSION["login_admin_error"] = true;
			}	
			
			
			if($success)
			{
			
					$_SESSION["user"] = serialize($user);
					
					if(!empty($_SESSION["login_admin_back"]))
					{
						header("Location: $_SESSION[login_admin_back]");
						exit();
					}
					else
					{	
						header("Location: index.php");
						exit();
					}
			}
			
		}
		
	}
	else
	{
		$_SESSION["login_admin_error"] = true;
	}

}
?>
<!DOCTYPE html>
<html>
<head><meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo _TITLE_ADMINISTRATOR; ?></title>
<link href="js/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="js/bootstrap/css/bootstrap-theme.min.css" type="text/css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/admin.css" />
<script language="JavaScript" type="text/javascript" src="js/const.php"></script>	
<!--[if lt IE 9]>
	<script src="js/bootstrap/html5shiv.js"></script>
	<script src="js/bootstrap/respond.min.js"></script>
<![endif]-->
</head><body style="padding-top:40px;" >
<div class="container" style="width:330px;">
<form method="post" action="login.php?login">
<?php
if(isset($_SESSION["login_admin_error"]))
{
	?>
	<div class="alert alert-danger"><?php echo _MESSAGE_ADMIN_DENIED?></div>
	<?php
	unset($_SESSION["login_admin_error"]);
}
?>
<div class="panel panel-primary">
		<div class="panel-heading">
	<h3 class="panel-title" style="text-align:center;"><?php echo _TITLE_ADMINISTRATOR; ?></h3></div>
	<div class="panel-body">
<table class="table" >	
<tbody>
<tr>
	<td><input type="text" maxlength="50" placeholder="<?php echo _USERNAME; ?>" required autofocus name="login" class='form-control' /></td>
</tr>
<tr>
	<td><input type="password" maxlength="50" placeholder="<?php echo _PASSWORD; ?>" required  name="passw" class='form-control' /></td>
</tr>
<?php if(count($db_array) > 1) {?>
<tr>
	<td><select required  class='form-control'  name="db">
	<option value=""><?php echo _DATABASE; ?></option>
	<?php
	$z = 0;
	foreach(array_keys($db_array) as $current)
	{
		$z++;
		?>
		<option <?php echo ($z == 1 ? "selected='selectd'" : ""); ?> value="<?=$current?>"><?php echo $db_array[$current][2]; ?></option>
		<?php
	}
	?>
	</select></td>
</tr>
<?php } ?>
</tbody>
<tfoot>
<tr>
	<td  ><input type="submit" class="btn btn-lg btn-info btn-block" value="<?php echo _BUTTON_ENTER; ?>" /></td>
</tr>
</tfoot>
</table>
<div style="text-align:center;"><small><?php echo $version_info; ?></small></div>
</div>
</div>
<?php if(count($db_array) == 1) {?>
<input type="hidden" name="db" value="0" />
<?php } ?>
<input type="hidden" name="token" value="<?php echo createFormToken("login");?>" />
</form>
</div>
<script  type="text/javascript" src="js/jquery.js?v=2.1.14"></script>	
<script type="text/javascript" src="js/bootstrap/bootstrap.min.js?v=3.3.5"></script>
</body>
</html>
<?php } ?>