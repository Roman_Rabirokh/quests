<?php if(!defined("_APP_START")) { exit(); }

$id =  isset($_GET['id']) ? (int)$_GET['id'] : 0;
$level  = isset($_POST['level']) ? (int)$_POST['level'] : 0;
$count  = 1;

if($id)
{
  $data['items']  = dbQuery('SELECT l.* FROM #__quest_levels AS l 
    INNER JOIN #__content
    AS c ON (l.quest_id=c.id)
    WHERE c.id  = :id', array(':id'  =>  $id));
    if(empty($data['items']))
    {
      $data['items'][0]=array();
      if($level)
        $count  = $level;
    }
    foreach($data['items'] as &$item)
    {
      $tmp=dbGetRow('SELECT originalname FROM #__images WHERE id =:id',array (':id'=>$item['image'])); 
      $item['originalname'] = $tmp['originalname'];
   
    }
    unset($item);
}
else
{
  $data['items'][0]=array();
  if($level)
    $count  = $level+1;
  }
$length = count($data['items']);
include('template.php');
