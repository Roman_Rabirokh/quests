<?php
global $user;


// Пагинация
$pages = isset($_GET['page']) ? $_GET['page'] - 1 : 0; // страница
$limit = 3; // количество отображаеммых квестов  на одной странице
$start = $limit * $pages;
$countPages = 0;

$data['ITEMS'] = dbQuery("SELECT c.name, c.detail_text, c.userid, c.mainimage, c.active AS status, c.url, c.id, cdata.*, city.name AS city FROM #__content
  AS c
  INNER JOIN #__content_data_2
  AS cdata ON(c.id=cdata.id)
  LEFT OUTER JOIN #__content
  AS city ON(cdata.f5=city.id)
  WHERE c.content_type = :cType AND c.userid= :userId
  ORDER BY c.showorder
  LIMIT :start , :end",
  array(':cType'  =>  2, ':userId'=> $user->getID(), ':start' => $start, ':end' => $limit));

$countPages = dbGetOne('SELECT COUNT(c.id)  
  FROM #__content AS c
  WHERE c.content_type = :cType AND c.userid= :userId
  ORDER BY c.showorder', array(':cType' => 2, 'userId' => $user->getID()));


$sum= dbGetOne('SELECT SUM(i.price) FROM #__invoices AS i '
	    . 'INNER JOIN #__content AS c ON(c.id=i.quest_id)'
	    . 'INNER JOIN #__users AS u ON(u.id=c.userid)'
	    . 'WHERE u.id= :id', array(':id' => $user->getID()));
    if(empty($sum))
	$sum=0;
$data['all_price']=$sum;

$sum= dbGetOne('SELECT SUM(i.sum_paid_user) FROM #__invoices AS i '
	    . 'INNER JOIN #__content AS c ON(c.id=i.quest_id)'
	    . 'INNER JOIN #__users AS u ON(u.id=c.userid)'
	    . 'WHERE u.id= :id', array(':id' => $user->getID()));
    if(empty($sum))
	$sum=0;
$data['all_price_percent']=$sum;

$data['percent']= dbGetOne('SELECT percent FROM #__users WHERE id= :id', array(':id' =>$user->getID()));
if(empty($data['percent']))
    $data['percent']=0;

foreach($data['ITEMS'] as &$item)
{
    if($item['mainimage'])
	$item['thumb']  = getImageById($item['mainimage'],array('height'=>381, 'width'=>333));
    else
	$item['thumb']  ='/templates/img/default-thumbnail.jpg';
  $item['url']  = Content::contentUrl($item['url']);
  $item['detail_text']=mb_substr($item['detail_text'],0,100).'...'; 
  $item['rating']= (int)dbGetOne('SELECT AVG(rating) FROM #__invoices WHERE 	quest_id= :id AND rating>0', [':id' => $item['id']]);
  

}
unset($item);
// Пагинация, выборка отоборжаемых номеров страниц
$countViewPages = 5; // количество отображаемых страниц

$pages++; // прибавляем 1 для отображения.
if ($countPages > $limit)
    $countPages = ceil($countPages / $limit);
else
    $countPages = 1;



if (($pages + 2) >= $countPages) {
    $endPage = $countPages;
} else {
    if (($pages + 2) >= $countViewPages)
	$endPage = $pages + 2;
    else
	$endPage = $countViewPages;
}
if (($pages - 2) < 2) {
    $startPage = 1;
} else {
    $startPage = $pages - 2;
    if (($endPage - $startPage) < $countViewPages && (($pages - 2) > 2)) {
	$startPage = $endPage - 4;
    }
}
unset($item);
include "template.php";
//
//':userid'=> $user->getID()