

	<div class="container">


		<div class="list-q">

		    <div>
			<h4 class="text-center hr_line">Информация по квестам:</h4>
				<ul class="text-center quests_info_list">
					<li>Оплачено за Ваши квесты: <span class="red_text"><?=$data['all_price']?></span> руб., </li>
					<li>Ваше вознаграждение : <span class="red_text"><?=$data['all_price_percent']?></span> руб., </li>
					<li>Вам выплачено:(без оплаты не работает) <span class="red_text">...</span> руб </li>
					<li>Ваш процент по вознаграждениям: <span class="red_text"><?=$data['percent']?></span>%</li>
				</ul>
			<br/>
			<br/>
			<br/>
			<h4 class="text-center hr_line">Ваши квесты:</h4>
			<br/>
		    </div>
      <?php foreach($data['ITEMS'] as $item)
      { ?>
			<div class="row block">
				<div class="col-md-3 left">
					<div class="img" style="background:url(<?= $item['thumb'] ?>) no-repeat center;background-size: 100%,cover;"></div>
					<div class="round-price"><span><strong><?=$item['f4'];?></strong> руб.</span></div>
				</div>
				<div class="col-md-9 right">
					<div class="row one">
					    <div class="col-lg-8 col-md-7">
							<h4><?=$item['name'];?></h4>

							<h3>г.<?=$item['city'];?></h3>
							<?=htmlspecialchars_decode($item['detail_text']);?>
						</div>
						<div class="col-lg-4 col-md-5 buttons created-quests-list-block-button text-right">
                            <div class="rate"> Рейтинг квеста: <?=$item['rating']?> </div>
						<?php if($item['status']==1){ ?>
						  <div><div class="text_wborder"> Опубликован</div></div>
						<?php } else if($item['status']==0) { ?>
						<button qid="<?=$item['id']?>" class="btn btn-md btn-default edit public_quest">ОПУБЛИКОВАТЬ</button>
						<?php } else if($item['status']==2) { ?>
						<div class="text_wborder">Отправлен на модерацию</div>
						<?php } else if($item['status']==3) { ?>
						<div class="text_wborder">Модерацию не прошел, отредактируйте квест</div>
						<?php } ?>
							<a href="/edit/quest?id=<?=$item['id']?>"><button class="btn btn-md btn-default edit">РЕДАКТИРОВАТЬ</button></a>
<!--							<button  class="btn btn-md btn-default clear" id="<?=$item['id']?>">УДАЛИТЬ</button>-->
						</div>
					</div>
					<div class="row two">
						<div class="col-lg-6">
							<div class="icons clearfix">
								<div class="col col-md-4">
									<div class="time icon-mini">
										<p><i class="icon-wall-clock"></i>Время <strong><?=$item['f1'];?></strong></p>
									</div>
								</div>
								<div class="col col-md-4">
									<div class="group icon-mini">
										<p><i class="icon-multiple-users-silhouette"></i>Участников <strong><?=$item['f2'];?></strong></p>
									</div>
								</div>
								<div class="col col-md-4">
									<div class="distance icon-mini">
										<p><i class="icon-speedometer"></i>Растояние <strong><?=$item['f3'];?></strong></p>
									</div>
								</div>
							</div>
							<div class="other-info">
								<span>Тип игры: <strong>Командный</strong></span>
								<span>Зарегистрировано участников:  <strong>5</strong> из <strong>10</strong></span>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="marshrut">
								<div class="row">
									<div class="col-md-2"><div class="icon-start-and-finish"></div></div>
									<div class="col-md-10">
										<div class="row">
											<div class="col-md-5">Начало маршрута:</div>
											<div class="col-md-7"><?=$item['f7']?></div>
										</div>

									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<hr>
      <?php } ?>
		             <?php if($endPage>1) { ?>
  <div class="col col-xs-12" id="pagination">
    <ul class="pagination">

      <li <?php if($pages==$startPage) {?> class="disable" <? } ?> > <a href="<? if($pages!=$startPage){ $tmp=$pages-1; echo ("?page=".$tmp); } else echo "#";?>" class="icon-caret-prev"> </a></li>
      <?php for($i=$startPage; $i<=$endPage; $i++)
      { ?>
	  <li <?php if($i==$pages) {?>  class="disable"  <? }?> ><a href="?page=<?echo($i)?>"><?=$i ?></a></li>

   <?php  } ?>
	  <li <?php if($pages==$endPage) {?> class="disable" <? } ?> ><a href="<?php $tmp=$pages+1; if($pages!=$endPage) { echo ("?page=".$tmp); } else echo "#"; ?>" class="icon-caret-next"> </a></li>
    </ul>
  </div>
 <?php } ?>
		</div>
	</div>
