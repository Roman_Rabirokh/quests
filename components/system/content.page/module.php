<?php if(!defined("_APP_START")) { exit(); } 
$pageUrl = "";
if(isset($params["URL"]))
{
	$pageUrl = $params["URL"];
}
else
{
	$cache_key = md5($_SERVER["REQUEST_URI"]);
	
	if($_SERVER["REQUEST_URI"] == "" || $_SERVER["REQUEST_URI"] == "/")
	{
		$pageUrl = "index";
		$page->isIndex = TRUE;
	}
	else
	{
		$mainPart = explode("?",$_SERVER["REQUEST_URI"]);
		
		if(substr($mainPart[0],0,1) == "/")
		{
			$pageUrl = substr($mainPart[0],1);
		}
		
		
		if(substr($pageUrl,count($pageUrl)-2) == "/")
		{
			$pageUrl = substr($pageUrl,0,count($pageUrl)-2);
		}
	}
}

if($pageUrl == "")
{
	$pageUrl = 'index';
	$page->isIndex = TRUE;
}

$data = Content::GetByUrl(urldecode($pageUrl));

if(empty($data))
{
	return false;
}
else
{	
	$page->meta[] = $data["meta_head"];
	$page->meta_title = $data["meta_title"] != "" ? $data["meta_title"] : $data["name"];
	$page->meta_description = $data["meta_description"];
	$page->meta_keywords = $data["meta_keywords"];
	$page->h1 = $data["meta_h1"] != "" ? $data["meta_h1"] : $data["name"];
	$page->url = $data["url"];
	$page->classes[] = 'content-type-' . $data['content_type'];
	$page->classes[] = 'content-' . $data['id'];
	
	
	

	$path = array();
	
	
	if(!empty($data['content_type_url']))
	{
		$parentPage = Content::GetByUrl($data['content_type_url']);
		$page->addPathItem($parentPage['name'],Content::contentUrl($parentPage['url']));
	}
	
	
	Content::getOpenContentTree($data['id'],$path);
	$path = array_reverse($path,TRUE);
	foreach($path as $key=>$value)
	{
		if($key == $data['id'])
		{
			$page->addPathItem($value['NAME']);
		}
		else
		{
			$page->addPathItem($value['NAME'],$value['LINK']);
		}
	}
	
	if($data["protected"] == 1 && !$user->Authorized())
	{
		$this->IncludeComponent("system/auth");
	}
	else
	{
		if($page->isIndex && file_exists(_TEMPL . "home.php"))
		{
			include(_TEMPL . "home.php");
		}
		else
		{
			if(file_exists(_TEMPL . "content-" . $data["id"] . ".php"))
			{
				include(_TEMPL . "content-" . $data["id"] . ".php");	
			}
			else if(file_exists(_TEMPL . "content-type-" . $data["content_type"] . ".php"))
			{
				include(_TEMPL . "content-type-" . $data["content_type"] . ".php");	
			}
			else
			{
				@include(_TEMPL . "content.php");
			}
		}
	}
	
	return true;
}