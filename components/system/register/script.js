$(function(){

	$("#btnRegister").click(function(){
		$("#btnRegister").attr("disabled","disabled");
		var customFields = $("#registerForm").serialize();
		// alert("/component/load/system/register?action=register" + (customFields != "" ? "&"+ customFields : ""));
		$.post("/component/load/system/register?action=register" + (customFields != "" ? "&"+ customFields : ""),{
			email:$("#tbREmail").val(),
			password:$("#tbRPassw").val(),
			repassword:$("#tbRRePassw").val(),
			code:$("#tbRCode").val(),
			token:$("#tbRToken").val()
		},function(data){
			$("#btnRegister").removeAttr("disabled");
			if(!data.RESULT)
			{
				$("#registerMessages").html(data.MESSAGE);
				$("#registerMessages").removeClass('alert alert-success');
				$("#registerMessages").addClass('alert alert-danger');
			}
			else
			{
				$("#registerMessages").html(data.MESSAGE);
				$("#registerMessages").addClass('alert alert-success');
				$("#registerMessages").removeClass('alert alert-danger');
				$("#registerForm").remove();
				$("body").animate({scrollTop:0}, '500', 'swing');
			}

		},"json");
		return false;
	});
});

$(document).ready(function(){
	// $('#registerDiv').hide();
	// $('#registration').on('click', function(){
	// 	$('#registerDiv').show('slow');
	// });
	$('#authorization').on('click', function(){
		window.location.href = "/login/";
	});
	$('#exit').on('click',function(){
		window.location.href = "/";
	});
});
