<?php if(!defined("_APP_START")) { exit(); }
if(!$user->Authorized())
{
?>
<div  class="container" role="main">
  <div class="row">
    <div class="col-sm-6 col-sm-offset-3 container-registerdiv">
      <br />
      <br />
<button id="registration">Регистрация на сайте</button>
<button id="authorization">Авторизация на сайте</button>
<button id="exit"> Выход на главную</button><br><br>
<div  id="registerMessages"></div>
  <div id="registerDiv">
    <form  method="post" id="registerForm">
      <legend>Регистрация</legend>
    	<input type="hidden"  id="tbRToken" value="<?php echo $data["TOKEN"]; ?>" />
    	<div class="form-group">
    		<label class="control-label"><?php echo $MSG["CAPTION_R_EMAIL"]; ?>:</label>
    		<input class="pull-left form-control" maxlength="100" type="email" required id="tbREmail"  />
    	</div>

    	<div id="customFields">
    		<?php echo $data["CUSTOM_FIELDS"]; ?>
    	</div>
    	<div class="row">
    	<div class="form-group col-md-12"><label class="control-label"><?php echo $MSG["CAPTION_R_PASSWORD"]; ?>:</label>
    	   <input type="password" class="pull-left form-control"  maxlength="100" required  id="tbRPassw"  />
    	</div>
    	<div class="form-group col-md-12"><label class="control-label"><?php echo $MSG["CAPTION_R_REPASSWORD"]; ?>:</label>
    	   <input  class="form-control" type="password"  maxlength="100" required  id="tbRRePassw"  /></div>
    	<div class="form-group col-md-6">
    	   <?php $this->includeComponent("system/captcha",array("SHOW"=>TRUE));?><br />
    	</div>
    	<div class="form-group col-md-6">
    	<label class="control-label"><?php echo $MSG["CAPTION_R_IMAGECODE"]; ?>:</label>
    	 <input  type="number" maxlength="6"  class="pull-left form-control" required  id="tbRCode"  /></div>
    	</div>

    	<input type="button" class="btn btn-warning" id="btnRegister" value="Регистрация" />
    </form>
  </div>
<br />
<br />
</div></div></div>
<?php } else {

	redirect("/personal/");
}
