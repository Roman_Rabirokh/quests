<?php if(!defined("_APP_START")) { exit(); } 


if(isset($params["SHOW"]))
{
	include("messages.php");
	$data["ID"] = uniqid();
	include("template.php");	
}
else
{
	include_once $_SERVER['DOCUMENT_ROOT'] . $this->getComponentPath("system/captcha") . 'sources/securimage.php';
	$securimage = new Securimage();
	return $securimage->check($params["CODE"]) ;
}