<?php if(!defined("_APP_START")) { exit(); }
// print_r($params); die;
if(isset($params["action"]))
{
	$result = array("RESULT"=>FALSE,"MESSAGE"=>"");

	if($params["action"] == "login")
	{
		if($user->Authorize($params["email"],$params["passw"]))
		{
			if (!isset($_SESSION['time_zone'])){
			  $_SESSION['time_zone'] = $params['time_zone'];
			  $result ["RESULT"] = TRUE;
			}
			if(!empty($_SESSION['quest']) && !empty($_SESSION['price'])){
				$userID = $user->getID();
				$invoiceID = dbGetOne('SELECT id
					FROM #__invoices
					WHERE user_id = :user_id
					AND quest_id = :quest_id',
					array(
						':user_id'=>$userID,
						':quest_id'=>$_SESSION['quest']));
				if(empty($invoiceID)){
					dbNonQuery('INSERT INTO #__invoices
					(user_id, price, date_create, quest_id)
					VALUES(:userid, :price, NOW(), :qid)',
					array(
						':userid'=>$userID,
						':price'=>(float)$_SESSION['price'],
						':qid'=>(int)$_SESSION['quest']
					));
				}
			}
		}
	}
	if($params["action"] == "restore")
	{
		include("messages.php");
		if(!validateEmail($params["email"]))
		{
			$result["MESSAGE"] = $MSG["RESTORE_EMAIL_ERROR"];
		}
		else
		{
			$findEmail = dbGetRow("SELECT email,controlstring FROM " . _DB_TABLE_PREFIX . "users WHERE email = :email",array(":email"=>$params["email"]));
			if($findEmail["email"] != "")
			{
				$url = "https://"._SITE . "/component/load/system/restore.password?control=" . $findEmail["controlstring"];
				$this->sendEmailTemplate(_SITE,_DEFAULT_EMAIL_FROM,"",$findEmail["email"],$MSG["RESTORE_EMAIL_SUBJECT"] . _SITE,"_restore.php",array("#URL#"=>$url));
				$result["RESULT"] = TRUE;
			}
			else
			{
				$result["MESSAGE"] = $MSG["RESTORE_EMAIL_NOT_FOUND"];
			}
		}
	}
	if ( $params["action"] == "logout")
	{
		$user->Logout();
		redirect("https://" . _SITE);
	}
	echo json_encode($result);
}
else
{
	include("messages.php");
	$page->addJS($this->getComponentPath($name) . "script.js");
	if(file_exists(_TEMPL . 'auth.php'))
	{
		include(_TEMPL . 'auth.php');
	}
	else
	{
		include("template.php");
	}
}
