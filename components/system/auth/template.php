<?php if(!defined("_APP_START")) { exit(); }

// echo "<pre>";
// print_r($_SESSION);
// echo "</pre>";

?>
<div  class="container" role="main">
  <div class="row">
    <div class="col-sm-6 col-sm-offset-3 container-registerdiv">
      <br />
      <br />
	<div class="text-center">
<button id="registration">Регистрация на сайте</button>
<button id="authorization">Авторизация на сайте</button>
<button id="exit"> Выход на главную </button>
</div>
<br><br><br><br>
<div>
  <form method="post" id="loginForm">
    <legend>Авторизация</legend>
  	<div class="form-group">
  	<label><?= $MSG["TITLE_EMAIL"] ?>:</label>
  	<input class="form-control" required type="email" id="tbEmail" name="email" />
  	</div>
  	 <div class="form-group">
  	<label><?= $MSG["TITLE_PASSWORD"] ?>:</label>
  	<input class="form-control" required type="password" id="tbPassw" name="passw" />
  	</div>
  	<input id="btnLogin" class="btn btn-warning" type="button" value="Вход" />
  </form>
</div>
<br />
<a href="#" id="rPassword"><?= $MSG["TITLE_RESTORE_PASSWORD"] ?></a>
<br /><br />
<div class="form-inline" id="rPassword_block" style="display:none;">
	 <div class="form-group"><input class="form-control" id="tbRestoreEmail" type="email" placeholder="<?= $MSG["RESTORE_EMAIL"] ?>" />&nbsp;&nbsp;<input class="btn btn-warning" type="button" value="Восстановить" id="btnRestorePassword" /></div>
</div>
<br />
<br />
</div></div></div>
