
<div id="succes_game" id-invoice="" class="modal fade">
    <div class="modal-dialog">
	<div class="modal-content">
	    <div class="modal-header">
		<button class="close" type="button" data-dismiss="modal" id="close_redirect">×</button>
		<h4 class="modal-title">Поздравляем Квест пройден</h4>
	    </div>
	    <div class="modal-body">
		<!-- FORM BODY BUY NEW GAME -->
			<button class="btn btn-lg btn-default submit">Хорошо</button>
		<!-- FORM BODY END -->

	    </div>
	    <div class="modal-footer">
		<button class="btn btn-default" type="button" data-dismiss="modal">
		    Закрыть
		</button>
	    </div>
	</div>
    </div>
</div>


<div id="show_answer" id-invoice="" class="modal fade">
    <div class="modal-dialog">
	<div class="modal-content">
	    <div class="modal-header">
		<button class="close" type="button" data-dismiss="modal" id="close_redirect">×</button>
		<h4 class="modal-title"> Если вы нажмете на данную кнопку, вы будете оштрафованы на 15 мин</h4>
	    </div>
	    <div class="modal-body">
		<!-- FORM BODY BUY NEW GAME -->
			<button type="button" class="btn btn-default submit" data-dismiss="modal">Отмена</button>
        <button type="button" class="btn btn-primary submit button-modal-green" id="show_answer_button" level='<?= $data['item']['number_level']; ?>' idGame='<?= $data['item']['id']; ?>'>Продолжить</button>
		<!-- FORM BODY END -->

	    </div>
	    <div class="modal-footer">
		<button class="btn btn-default" type="button" data-dismiss="modal">
		    Закрыть
		</button>
	    </div>
	</div>
    </div>
</div>


<article class="mt-content">
    <div class="container">
	<div class="party">
	    <h2>ВЫ ПРИНИМАЕТЕ УЧАСТИЕ</h2>
		<article class="mt-content-quest">
	    <div class="row">
		<div class="col-lg-3 col-sm-12 col-md-4 left-sidebar quest-image-block">
      <img   src="<?= $data['item']['thumb']; ?>" alt="">
			<img   src="<?= $data['item']['thumb_level_image']; ?>" alt="">
			<img   src="<?= $data['item']['thumb_image_additional']; ?>" alt=""></div>
		<div class="col-lg-9 col-sm-12 col-md-8 container-content">
		    <h3>КВЕСТ - “<?= $data['item']['name']; ?>”</h3>
                    <a href="<?=$data['url']?>" target="_blank">   Подробное Описание квеста  </a>
		    <div class="detail_text">Описание: <?= $data['item']['detail_text']; ?> </div>
		    <div class="task">Задание: <?= $data['item']['task']; ?> </div>
		    <div class=level>Уровень: <?= $data['item']['number_level']; ?> </div>
		    <div id='game_panel' <?php if (empty($data['item']['timer'])) { ?> style='display:none;' <?php } ?>>

			<div class="row one">
			    <div class="col col-lg-12 col-md-12">НОВАЯ ПОДСКАЗКА БУДЕТ ДОСТУПНА ЧЕРЕЗ:</div>
			    <div class="col col-lg-12 col-md-12 time" id="timer"></div>
			</div>
			<div class="row two">
			    <div class="col col-lg-7 col-md-6"><input type="text" class="form-control" id="answer" placeholder="Введите ответ чтобы перейти на следующий уровень квеста"></div>
		<div class="col col-lg-5 col-md-6"><button class="btn btn-default next" data-toggle="modal" data-target="#show_answer" id="show_answer" level='<?= $data['item']['number_level']; ?>' idGame='<?= $data['item']['id']; ?>'>Показать ответ</button>
		<button class="btn btn-default help" id="send_answer" level='<?= $data['item']['number_level']; ?>' idGame='<?= $data['item']['id']; ?>'>Проверить ответ</button>
			    <button class="btn btn-default help" id="stop_game" level='<?= $data['item']['number_level']; ?>' idGame='<?= $data['item']['id']; ?>'>Остановить игру</button></div>
			    <input type="hidden" name="id_game" value="<?= $_GET['id'] ?>">
			</div>

		    </div>
		    <div class="row three">
			<?php if (!empty($data['item']['hint_1'])) { ?>
    			<div class="col col-lg-7 col-md-6">Подсказка №1:  <?= $data['item']['hint_1'] ?></div>
    			<div class="col col-lg-5 col-md-6 btn-wrapper"><button class="btn btn-default help" level='<?= $data['item']['number_level']; ?>' idGame='<?= $data['item']['id']; ?>' hintId='1' id='send_accept_hint'  <?php if ((empty($data['item']['timer']) && !empty($data['item']['hint_2'])) || !empty($data['item']['timer'])) { ?> style='display:none;' <?php } ?>>Ок</button></div>
			<?php } if (!empty($data['item']['hint_2'])) { ?>
    			<div class="col col-lg-7 col-md-6">Подсказка №2:  <?= $data['item']['hint_2'] ?></div>
    			<div class="col col-lg-5 col-md-6 btn-wrapper"><button class="btn btn-default help" level='<?= $data['item']['number_level']; ?>' idGame='<?= $data['item']['id']; ?>' hintId='2' id='send_accept_hint'  <?php if ((empty($data['item']['timer']) && !empty($data['item']['hint_3'])) || !empty($data['item']['timer'])) { ?> style='display:none;' <?php } ?>>Ок</button></div> <?php } ?>
			<div <?php if (empty($data['item']['hint_3'])) { ?> 	style="display:none" <? } ?> >
			    <div class="col col-lg-7 col-md-6">Ответ :<span id="answer_wright">  <?= $data['item']['hint_3'] ?> </span></div>
			    <div class="col col-lg-5 col-md-6 btn-wrapper"><button class="btn btn-default help" id='game_next_level' level='<?= $data['item']['number_level']; ?>' idGame='<?= $data['item']['id']; ?>'>Перейти на следующий уровень</button></div></div>


		    </div>

		    <?php // print_r('result-test-button: '); print_r( !empty($data['item']['timer']));  ?>
		    <br/>Введенные ответы : <div id="print_send_answers"></div>
		</div>
	    </div>
		</article>
	</div>
    </div>
</article>
<?php if (!empty($data['item']['timer'])) { ?>
    <script>

        var timeend = new Date('<?= $data['item']['timer']->format('Y-m-d H:i:s'); ?>');
    </script>
<?php } ?>
