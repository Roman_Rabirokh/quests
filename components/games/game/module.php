<?php

//print_r('<pre>');
//print_r('function: <br>');
//print_r(function_exists('isNextLevelNoStart'));
//print_r('</pre>');
//print_r('GAME');
$id = (isset($_GET['id'])) ? $_GET['id'] : 0;
$offset_time = (isset($_SESSION['time_zone'])) ? $_SESSION['time_zone'] : +2;

ini_set('display_errors', 0);
ini_set('display_startup_errors', 0);

$date = new DateTime("now", new DateTimeZone('UTC'));
$date->add(new DateInterval('PT' . $offset_time . 'H'));
//$date =  $date->format('Y-m-d H:i:s');


if ($id) {
// получаем данные игры: данные квеста и текущий уровень игры

    $data['item'] = dbGetRow('SELECT invoice.id, quest.name, quest.detail_text, quest.mainimage, game_levels.start,quest.id AS quest_id,
    game_levels.number_level, game_levels.id AS game_level_id, quest_levels.image AS level_image, 
    game_levels.time_hint_1 AS game_time_hint_1, game_levels.time_hint_2 AS game_time_hint_2, game_levels.end AS end, 
    quest_levels.hint_1, quest_levels.hint_2, quest_levels.hint_3,
    quest_levels.time_hint_1 AS quest_level_time_hint_1, quest_levels.time_hint_2 AS quest_level_time_hint_2, quest_levels.time_hint_3 AS quest_level_time_hint_3, 
    quest_levels.task
    FROM #__invoices
    AS invoice
    INNER JOIN #__game_levels
    AS game_levels ON(invoice.id=game_levels.invoice_id)
    INNER JOIN #__content
    AS quest ON (invoice.quest_id=quest.id)
    INNER JOIN #__quest_levels
    AS quest_levels ON(quest_levels.quest_id=quest.id)
    WHERE quest.content_type =  :content_type
    AND (game_levels.start IS NOT NULL AND game_levels.start!="0000-00-00 00:00:00") AND (game_levels.end IS NULL OR game_levels.end="0000-00-00 00:00:00")
    AND quest_levels.level  = game_levels.number_level AND invoice.id = :id AND invoice.status>= :status_pay'
	    , array(':content_type' => 2, ':id' => $id, ':status_pay' => 1));
    if (!empty($data['item'])) {
	// определяем сколько времени прошло
	$start = new DateTime($data['item']['start'], new DateTimeZone('UTC'));
	$interval = $start->diff($date);
	$diff = $interval->format('%i');
	// удаляем подсказки которым ещё не пришло время
	if ($diff < $data['item']['quest_level_time_hint_1']) { // 1 подсказка
	    unset($data['item']['hint_1']);
	    unset($data['item']['hint_2']);
	    unset($data['item']['hint_3']);
	    $data['item']['time'] = $data['item']['quest_level_time_hint_1'];
	    $data['item']['start'] = $data['item']['start'];
	} else { //2 подсказка
	    if ($data['item']['game_time_hint_1'] != 0) {
		$start = new DateTime($data['item']['game_time_hint_1'], new DateTimeZone('UTC'));
		$interval = $start->diff($date);
		$diff = $interval->format('%i');

		if ($diff < $data['item']['quest_level_time_hint_2']) {
		    unset($data['item']['hint_2']);
		    unset($data['item']['hint_3']);

		    $data['item']['time'] = $data['item']['quest_level_time_hint_2'];
		    $data['item']['start'] = $data['item']['game_time_hint_1'];
		} else { // 3 подсказка
		    if ($data['item']['game_time_hint_2'] != 0) {
			$start = new DateTime($data['item']['game_time_hint_2'], new DateTimeZone('UTC'));
			$interval = $start->diff($date);
			$diff = $interval->format('%i');

			if ($diff < $data['item']['end']) {
			    unset($data['item']['hint_3']);

			    $data['item']['time'] = $data['item']['end'];
			    $data['item']['start'] = $data['item']['game_time_hint_2'];
			} else { // кнопка переход на следующий уровень
			    //TODO: не отображать кнопку если игра закончилась.
			}
		    }
		}
	    } else {
		unset($data['item']['hint_2']);
		unset($data['item']['hint_3']);
	    }
	}


//	else if($diff<$data['item']['quest_level_time_hint_3'])
//	{
//	    unset($data['item']['hint_3']);
//	    if($data['item']['game_time_hint_2']!=0){
//	    $data['item']['time']=$data['item']['quest_level_time_hint_3'];
//	    $data['item']['start']=$data['item']['game_time_hint_2'];
//	    }
//	}
    } else {
	if (isNextLevelNoStart($id))
	    $data['item'] = dbGetRow('SELECT invoice.id, quest.name, quest.detail_text, quest.mainimage, game_levels.start,quest.id AS quest_id,
		    game_levels.number_level, game_levels.id AS game_level_id, quest_levels.image AS level_image, 
		    game_levels.time_hint_1 AS game_time_hint_1, game_levels.time_hint_2 AS game_time_hint_2, game_levels.end AS end, 
		    quest_levels.hint_1, quest_levels.hint_2, quest_levels.hint_3,
		    quest_levels.time_hint_1 AS quest_level_time_hint_1, quest_levels.time_hint_2 AS quest_level_time_hint_2, quest_levels.time_hint_3 AS quest_level_time_hint_3, 
		    quest_levels.task
		    FROM #__invoices
		    AS invoice
		    INNER JOIN #__game_levels
		    AS game_levels ON(invoice.id=game_levels.invoice_id)
		    INNER JOIN #__content
		    AS quest ON (invoice.quest_id=quest.id)
		    INNER JOIN #__quest_levels
		    AS quest_levels ON(quest_levels.quest_id=quest.id)
		    WHERE quest.content_type =  :content_type
		    AND (game_levels.start IS NOT NULL AND game_levels.start!="0000-00-00 00:00:00") AND (game_levels.end IS NOT NULL OR game_levels.end!="0000-00-00 00:00:00")
		    AND quest_levels.level  = game_levels.number_level AND invoice.id = :id AND invoice.status>= :status_pay'
		    , array(':content_type' => 2, ':id' => $id, ':status_pay' => 1));
	else {
	    print_r('GAME_END');
	}
    }
//    print_r('<pre>');
//    print_r($data);
//    print_r('</pre>');
    if (!empty($data['item']['quest_id'])) {
	$data['item']['image_additional'] = dbGetOne('SELECT img.id FROM #__images AS img '
		. 'INNER JOIN #__content AS c ON (c.id=img.parentid) '
		. 'WHERE img.title= "image_additional" AND c.id= :id ', array(':id' => $data['item']['quest_id']));
    }
    $data['url']= dbGetOne('SELECT c.url FROM #__content AS c '
        . ' INNER JOIN #__invoices AS i ON(i.quest_id=c.id) '
        . ' WHERE i.id= :id ' ,array(':id' => $id));
    if(!empty($data['url']))
        $data['url']=$data['url'].'?idGame='.$id.'&action=show';
  if(!$data['item']['name'])
      redirect('/');
}




if ($data['item']['hints'])
    $data['item']['hints'] = explode('/', $data['item']['hints']);


$data['item']['thumb'] = getImageById($data['item']['mainimage'], array('height' => 250, 'crop'=>[0,0,263,228]));
$data['item']['thumb_level_image'] = getImageById($data['item']['level_image'], array('height' => 250, 'crop'=>[0,0,263,228]));
$data['item']['thumb_image_additional'] = getImageById($data['item']['image_additional'], array('height' => 250,'crop'=>[0,0,263,228]));

if ($data['item']['time']) {
    $data['item']['timer'] = new DateTime($data['item']['start']);
    $data['item']['timer']->add(new DateInterval('PT' . $data['item']['time'] . 'M'));
}


include 'template.php';
