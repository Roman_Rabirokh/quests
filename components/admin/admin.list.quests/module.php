<?php
$offset=isset($_POST['offset']) ? (int)$_POST['offset'] : 0;
$data['ITEMS'] = dbQuery("SELECT c.name, c.detail_text, c.mainimage, c.url, c.id, cdata.*, city.name AS city FROM #__content
  AS c
  INNER JOIN #__content_data_2
  AS cdata ON(c.id=cdata.id)
  LEFT OUTER JOIN #__content
  AS city ON(cdata.f5=city.id)
  WHERE c.content_type = :cType
  ORDER BY c.showorder
  LIMIT $offset,1",
  array(':cType'  =>  2));
foreach($data['ITEMS'] as &$item)
{
  $item['thumb']  = getImageById($item['mainimage'],array('height'=>381, 'width'=>333));
  $item['url']  = Content::contentUrl($item['url']);
}

$data['offset']=$offset;
unset($item);
include "template.php";
