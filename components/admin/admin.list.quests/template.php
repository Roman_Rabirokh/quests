<?php if(!$data['offset']) { ?>
<article class="mt-content">
	<div class="container">
		<div class="row navigation">
			<div class="col col-xs-12 zero">
				<a class="btn btn-default btn-lg icon-button" href="/add/quest/">ДОБАВИТЬ КВЕСТ</a>
				<a class="btn btn-default btn-lg icon-button">ТРАНЗАКЦИИ</a>
				<a class="btn btn-default btn-lg icon-button">ЖАЛОБЫ ( <span class="count">12</span> )</a>
			</div>
		</div>
		<div class="list-q">
		<?php } ?>
      <?php foreach($data['ITEMS'] as $item)
      { ?>
			<div class="row block">
				<div class="col-md-3 left">
					<div class="img"><img src="<?=$item['thumb']?>" alt=""></div>
					<div class="round-price"><span><strong><?=$item['f4'];?></strong> руб.</span></div>
				</div>
				<div class="col-md-9 right">
					<div class="row one">
						<div class="col-lg-10 col-md-9">
							<h2>г.<?=$item['city'];?></h2>
							<?=htmlspecialchars_decode($item['detail_text']);?>
						</div>
						<div class="col-lg-2 col-md-3">
							<a href="/admin/quest?id=<?=$item['id']?>"><button class="btn btn-md btn-default edit">РЕДАКТИРОВАТЬ</button></a>
							<button  class="btn btn-md btn-default clear" id="<?=$item['id']?>">УДАЛИТЬ</button>
						</div>
					</div>
					<div class="row two">
						<div class="col-lg-6">
							<div class="icons clearfix">
								<div class="col col-md-4">
									<div class="time icon-mini">
										<p><i class="icon-wall-clock"></i>Время <strong><?=$item['f1'];?></strong></p>
									</div>
								</div>
								<div class="col col-md-4">
									<div class="group icon-mini">
										<p><i class="icon-multiple-users-silhouette"></i>Участников <strong><?=$item['f2'];?></strong></p>
									</div>
								</div>
								<div class="col col-md-4">
									<div class="distance icon-mini">
										<p><i class="icon-speedometer"></i>Растояние <strong><?=$item['f3'];?></strong></p>
									</div>
								</div>
							</div>
							<div class="other-info">
								<span>Тип игры: <strong>Командный</strong></span>
								<span>Зарегистрировано участников:  <strong>5</strong> из <strong>10</strong></span>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="marshrut">
								<div class="row">
									<div class="col-md-2"><div class="icon-start-and-finish"></div></div>
									<div class="col-md-10">
										<div class="row">
											<div class="col-md-5">Начало маршрута:</div>
											<div class="col-md-7"><?=$item['f7']?></div>
										</div>

									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
      <?php } ?>
			<?php if(!$data['offset']) { ?>
			<button class="btn btn-default btn-lg icon-button all" id="showMoreQuests">ПОКАЗАТЬ БОЛЬШЕ</button>
		</div>
	</div>
</article>
		<?php } ?>
