<?php if(!defined("_APP_START")) { exit(); }
$id =  isset($_GET['id']) ? (int)$_GET['id'] : 0;
$data['item'] = dbGetRow('SELECT  c.*, cdata.*, city.id AS cityId, image.id AS imageiId,
  image.originalname AS imageName, file.id AS fileId, file.originalname AS fileName
  FROM #__content AS c
  LEFT OUTER JOIN #__content_data_2
  AS cdata ON(c.id=cdata.id)
  LEFT OUTER JOIN #__content AS city
  ON(city.id=cdata.f5)
  LEFT OUTER JOIN #__images
  AS image ON(image.parentid=c.id AND image.showorder  = :showorder )
  LEFT OUTER JOIN #__files
  AS file ON(file.parentid=c.id)
  WHERE c.id  = :id
  ', array(':id'  =>  $id, ':showorder' => 501));
$data['cities'] = dbQuery('SELECT id, name FROM  #__content
  WHERE content_type =  :type', array(':type' =>  3 ));

$data['transport']= dbQuery('SELECT id, name FROM #__content WHERE content_type = :type', array(':type'=> 6));

  // if(empty($data['item']))
  // {
  //     $this->actionResult = FALSE;
  //     redirect('/404/');
  // }
  // else
  // {	
    if($data['item']['mainimage'])
	$data['item']['thumb']  = getImageById($data['item']['mainimage'],array('height'=>381, 'width'=>333));
    else
	$data['item']['thumb']  ='/templates/img/default-thumbnail.jpg';
    $data['item']['detail_text']  = strip_tags(htmlspecialchars_decode($data['item']['detail_text']));
    $params = array();
    include('template.php');
  // }




?>
