<?php if(!defined("_APP_START")) { exit(); }?>

<article class="mt-content">
	<iframe name="_save_action" id="_save_action" style="display:none;"></iframe>
<form action="/save/quest/" method="post" name="form_quest" enctype="multipart/form-data">
	<input name="questID" value="<?php echo intval($_GET['id']); ?>" type="hidden">
	<input name="active" value="0" type="hidden">
	<div class="container">
		<div class="add-q">
			<div class="row one">
				<div class="col-lg-3 col-md-4">
					<div class="img"><img src="<?=$data['item']['thumb'];?>" alt="" id="img_main"></div>
					<div class="file_upload">
						<button type="button" id="button_select_image">Выбрать фото</button>
						<input type="file" name="main_image">
					</div>
				</div>
				<div class="col-lg-9 col-md-8">
					<div class="form-group foz16">
						<label for="usr"><span class="icon-mini-info"></span>Название:</label>
						<input type="text" class="form-control" id="usr" name="name_quest" value="<?=$data['item']['name'];?>">
					</div>
					<div class="form-group foz16"><span class="icon-mini-info"></span>
						<label for="comment">Описание квеста:</label>
						<textarea class="form-control" rows="5" id="comment" name="detail_text"><?=$data['item']['detail_text'];?></textarea>
					</div>
					<div class="row">
						<div class="col-md-12 data">
							<div class="mini time">
								<label>Cреднее время:</label>
								<div class="icon"></div>
								<input type="text" placeholder="5 ч 40м" name="time_quest" value="<?=$data['item']['f1'];?>">
							</div>
							<div class="mini group">
								<label>Участников:</label>
								<div class="icon"></div>
								<input type="text" placeholder="3" name="people_quest" value="<?=$data['item']['f2'];?>">
							</div>
							<div class="mini distance">
								<label>Растояние:</label>
								<div class="icon"></div>
								<input type="text" placeholder="12" name="length_quest" value="<?=$data['item']['f3'];?>">
								<span class="km">км</span>
							</div>
							<div class="mini price">
								<label>Цена:</label>
								<input type="text" name="price_quest" value="<?=$data['item']['f4'];?>">
								<span class="km">руб.</span>
							</div>
							<div class="mini calendar">
								<label>Дата:</label>
								<input type="text" name="date" placeholder="10 сентября 2016" id="datepicker" value="<?=$data['item']['f16'];?>">
								<div class="icon"></div>
								<label>В любой другой день<input type="checkbox"></label>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row two">
				<div class="col-lg-5 col-md-12">
					<div class="marsh row">
						<div class="col-md-2">
							<div class="icon"></div>
						</div>
						<div class="col-md-10">
							<div class="row">
								<div class="col-md-5">Начало маршрута:</div>
								<div class="col-md-7">
									<input type="text" name="start_route_quest" value="<?=$data['item']['f7'];?>">
								</div>
							</div>
              <div class="row">
								<div class="col-md-5">Конец маршрута:</div>
								<div class="col-md-7">
									<input type="text" name="end_route_quest" value="<?=$data['item']['f8'];?>">
								</div>
							</div>
							<div class="row">
								<div class="col-md-5">Изображение:</div>
								<div class="col-md-7">
									<?php if(!$data['item']['imageiId']) { ?>
									<input type="file" name="file_image" id="quest_image">
									<div></div>
									    <?php } else { ?>
										<?=$data['item']['imageName']?><button  name="<?=$data['item']['imageiId'] ?>" id="delete_image" >Удалить файл </button>
										<?php } ?>
								</div>
							</div>
							<div class="row">
								<div class="col-md-5">Звуковой файл:</div>
								<div class="col-md-7">
									<?php if(!$data['item']['fileId']) { ?>
										<input type="file" name="file_sound" id="quest_sound">
										<div></div>
									<?php } else { ?>
										<?=	$data['item']['fileName']?><button  name="<?=$data['item']['fileId'] ?>" id="delete_file" >Удалить файл </button>
										<?php } ?>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-7 col-md-12 m1">
					<div class="row col-xs-12 m0">
						<div class="col col-md-6 col-sm-6">
							Вид транспорта:
							<div class="dropdown">
								<!-- <a class="btn btn-default btn-xs dropdown-toggle caretka" type="button" data-toggle="dropdown" id="transport" aria-haspopup="true" aria-expanded="false">Без транспорта <span class="icon-caret"></span></a>
								<ul class="dropdown-menu" aria-labelledby="transport">
									<li><a href="#">Без транспорта</a></li>
									<li><a href="#">Пешком</a></li>
									<li><a href="#">На автомобиле</a></li>
								</ul> -->
							<select class="btn btn-default btn-xs dropdown-toggle caretka" name="transport">
					<?php foreach($data['transport'] as $transport) { ?>		    
					    <option value="<?php echo $transport['id'] ?>" <?php if($data['item']['f17']==$transport['id']) { ?> selected <?php } ?>> <?php echo $transport['name'] ?></option>
					   <?php } ?>
							</select>
							</div>
						</div>
						<div class="col-md-6 col-sm-6">
							Время игры: от <input type="text" name="time_start" value="<?=$data['item']['f9'];?>"> до <input type="text"  name="time_end"value="<?=$data['item']['f15'];?>">
						</div>
					</div>

					<div class="row col-xs-12 m0">
						<div class="col col-md-6 col-sm-6">
							Город:
							<div class="dropdown">
								<!-- <a class="btn btn-default btn-xs dropdown-toggle caretka" type="button" data-toggle="dropdown" id="transport" aria-haspopup="true" aria-expanded="false">Без транспорта <span class="icon-caret"></span></a>
								<ul class="dropdown-menu" aria-labelledby="transport">
									<li><a href="#">Без транспорта</a></li>
									<li><a href="#">Пешком</a></li>
									<li><a href="#">На автомобиле</a></li>
								</ul> -->
							<select class="btn btn-default btn-xs dropdown-toggle caretka" name="city">
								<?php
								 foreach ($data['cities'] as $city) { ?>
								<option value="<?=$city['id'];?>" <?php if($city['id']==$data['item']['cityId']) { ?> selected <?php } ?>> <?=$city['name'];?> </option>
								<?php } ?>
 							</select>
							</div>
						</div>
						<div class="col-md-6 col-sm-6">
						</div>
					</div>


					<div class="row col col-xs-12 map">
						<div class="icon-map"></div> <span>Как добраться?</span>
						<div class="form-group">
							<input type="text" class="form-control" id="usr" name="location" value="<?=$data['item']['f6'];?>">
						</div>
					</div>
				</div>
			</div>
			<div class="row three tools">
				<div class="col col-xs-12">
					<span class="foz16">Выбор инструментов: <input type="text" class="form-control" name="tools" value="<?=$data['item']['f10'];?>"></span>
					<!-- <div class="c">
						<div class="dropdown">
							<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
							<i class="icon-photo"></i> Фотоаппарат
							<span class="icon-caret"></span>
							</button>
							<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
								<li><a href="#"><i class="icon-photo"></i> Фотоаппарат</a></li>
								<li><a href="#"><i class="icon-ligher"></i> Фонарик</a></li>
							</ul>
						</div>
						<div class="add-minus">
							<span></span><span></span>
						</div>
					</div>
					<div class="c">
						<div class="dropdown">
							<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
							<i class="icon-ligher"></i> Фонарик</a>
							<span class="icon-caret"></span>
							</button>
							<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
								<li><a href="#"><i class="icon-photo"></i> Фотоаппарат</a></li>
								<li><a href="#"><i class="icon-ligher"></i> Фонарик</a></li>
							</ul>
						</div>
						<div class="add-minus">
							<span></span><span></span>
						</div>
					</div>
					<div class="c">
						<div class="dropdown">
							<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
							<i class="icon-photo"></i> Фотоаппарат
							<span class="icon-caret"></span>
							</button>
							<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
								<li><a href="#"><i class="icon-photo"></i> Фотоаппарат</a></li>
								<li><a href="#"><i class="icon-ligher"></i> Фонарик</a></li>
							</ul>
						</div>
						<div class="add-minus">
							<span></span><span></span>
						</div>
					</div>
					<div class="c">
						<div class="dropdown">
							<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
							<i class="icon-ligher"></i> Фонарик</a>
							<span class="icon-caret"></span>
							</button>
							<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
								<li><a href="#"><i class="icon-photo"></i> Фотоаппарат</a></li>
								<li><a href="#"><i class="icon-ligher"></i> Фонарик</a></li>
							</ul>
						</div>
						<div class="add-minus">
							<span></span><span></span>
						</div>
					</div>
					<div class="c">
						<div class="dropdown">
							<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"><i class="icon-photo"></i>
							Фотоаппарат
							<span class="icon-caret"></span>
							</button>
							<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
								<li><a href="#"><i class="icon-photo"></i> Фотоаппарат</a></li>
								<li><a href="#"><i class="icon-ligher"></i> Фонарик</a></li>
							</ul>
						</div>
						<div class="add-minus">
							<span></span><span></span>
						</div>
					</div> -->
				</div>
			</div>
			<?php $this->includeComponent('users/quest.level.form',$params); ?>
		<div class="levels">
			<div class="col-xs-12 final">
				<div class="row">
					<div class="col-sm-6 col-xs-6">
					    <input type="submit" class="btn chern" value="В ЧЕРНОВИК">
					</div>
					<div class="col-sm-6 col-xs-6">
					    <input type="submit" class="btn add-q" value="ДОБАВИТЬ КВЕСТ">
					    
					</div>
				</div>
			</div>
		</div>
		</div>
	</div>
</form>
</article>
