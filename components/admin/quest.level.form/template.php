<?php if(!defined("_APP_START")) { exit(); }?>
<?php

foreach( $data['items'] as $item)
{

	$itemID = uniqid();
	if(!empty($item))
	{
			$itemID = $item['id'];
	}

	?>
<div class="levels">
				<div class="row">
					<div class="col-xs-12 lvl"><span class="count"><?=$count;?></span> уровень</div>
					<div class="col-xs-12 add-task">
						<div class="row">
							<div class="col-md-2"><span class="icon-info"></span>Название:</div>
							<div class="col-md-10">
								<div class="form-group">
									<input type="text" name="level[<?php echo $itemID;  ?>][name]" class="form-control" id="name_level" value="<?if($item) echo $item['name'];?>">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-2"><span class="icon-info"></span>Задание:</div>
							<div class="col-md-10">
								<div class="form-group">
									<textarea class="form-control" rows="5" name="level[<?php echo $itemID; ?>][task]"  id="task_level" ><?if($item) echo $item['task'];?></textarea>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-2 pds"><i></i>Подсказка №1:</div>
							<div class="col-md-10">
								<div class="row">
									<div class="col-md-9">
										<div class="form-group">
											<input type="text" class="form-control" name="level[<?php echo $itemID; ?>][hint_1]"  id="usr" placeholder="Напишите текстовую подсказку или вопрос на который надо дать ответ" value="<?if($item) echo $item['hint_1'];?>">
										</div>
									</div>
									<div class="col-md-3">
									<span class="vremya">Время:</span>
									<div class="form-group">
										<input type="text" class="form-control" id="usr" name="level[<?php echo $itemID; ?>][time_hint_1]" value="<?if($item) echo $item['time_hint_1'];?>">
									</div>
									<span class="min">минут</span>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-2 pds"><i></i>Подсказка №2:</div>
							<div class="col-md-10">
								<div class="row">
									<div class="col-md-9">
										<div class="form-group">
											<input type="text" class="form-control" id="usr" placeholder="Напишите текстовую подсказку или вопрос на который надо дать ответ" name="level[<?php echo $itemID; ?>][hint_2]" value="<?if($item) echo $item['hint_2'];?>">
										</div>
									</div>
									<div class="col-md-3">
									<span class="vremya">Время:</span>
									<div class="form-group">
										<input type="text" class="form-control" id="usr" name="level[<?php echo $itemID; ?>][time_hint_2]" value="<?if($item) echo $item['time_hint_2'];?>">
									</div>
									<span class="min">минут</span>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-2 pds"><i></i>Подсказка №3:</div>
							<div class="col-md-10">
								<div class="row">
									<div class="col-md-9">
										<div class="form-group">
											<input type="text" class="form-control" id="usr" placeholder="Напишите текстовую подсказку или вопрос на который надо дать ответ"  name="level[<?php echo $itemID; ?>][hint_3]" value="<?if($item) echo $item['hint_3'];?>">
										</div>
									</div>
									<div class="col-md-3">
									<span class="vremya">Время:</span>
									<div class="form-group">
										<input type="text" class="form-control time_hint_3" id="usr" name="level[<?php echo $itemID; ?>][time_hint_3]" value="<?if($item) echo $item['time_hint_3'];?>">
									</div>
									<span class="min">минут</span>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-2 otv"><i></i>Ответ:</div>
							<div class="col-md-10">
								<div class="form-group">
									<textarea class="form-control" rows="5" name="level[<?php echo $itemID; ?>][answer]" id="answer_level"><?if($item) echo $item['answer'];?></textarea>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-2 photo"><i></i>Загрузка изображения:</div>
							<div class="col-md-10">
								<div class="form-group">
								    
									<input type="file" name="level[<?php echo $itemID; ?>]">
								<?php if(!empty($item['originalname'])) 
								{
								    echo $item['originalname'];
								} ?>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xs-12 buttons">
						<div class="row">
							<div class="col-sm-6 col-xs-6">
								<button class="btn plus" <?php if($count!=$length) { ?> style="display:none;" <?php } ?> ><span class="icon"></span>Добавить уровень</button>
							</div>
							<div class="col-sm-6 col-xs-6">
								<button class="btn minus"><span class="icon"></span>Удалить уровень</button>
							</div>
						</div>
					</div>

				</div>
			</div>
 <?php $count++;  } ?>
