<?php
$idQuest = isset($params['idQuest']) ? $params['idQuest'] :  false;
$cityId=isset($params['cityId']) ? $params['cityId'] :  false;
if($idQuest)
{
  $data['SIMILARQUESTS']  = dbQuery('SELECT c.name, c.detail_text, c.mainimage, c.url, cdata.*, city.name AS city FROM #__content
    AS c
    INNER JOIN #__content_data_2
    AS cdata ON(c.id=cdata.id)
    INNER JOIN #__content
    AS city ON(cdata.f5=city.id)
    WHERE c.content_type  = :cType AND c.active  = :status AND c.id  <>  :idQuest AND city.id = :cityId
    ORDER BY c.showorder
    LIMIT 3;
  ',array(':cType'  =>  2,':status' =>  1,':idQuest' =>  $idQuest, ':cityId'  => $cityId));
  foreach($data['SIMILARQUESTS'] as &$item)
  {
    $item['thumb']  = getImageById($item['mainimage'],array('height'=>381, 'width'=>333));
    $item['url']  = Content::contentUrl($item['url']);
    $item['detail_text']= mb_substr($item['detail_text'], 0, 90, 'UTF-8').' ...';
  }
}
include 'template.php';
