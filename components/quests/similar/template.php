<div class="row other">
  <div class="list">
    <h2 class="other-title">Похожие квесты</h2>
    <?php foreach($data['SIMILARQUESTS'] as $quest)
    { ?>
    <div class="col col-lg-4 col-sm-6">
      <figure class="icon-big" style="background-image:url(<?=$quest['thumb']?>);    border: 2px solid #fff;
      border-image: url(../sprites/sprite_border.png) 2 stretch;
      border-radius: 3px;background-size:cover;" >
        <!-- <img src="<?php //echo $quest['thumb']?>" alt=""> -->
        <div class="top">г.<?=$quest['city'];?></div>
        <div class="center">
          <a class="btn btn-default btn-lg" href="<?=$quest['url']?>">ВЫБРАТЬ КВЕСТ</a>
        </div>
        <div class="bottom">
          <div class="clearfix desc">
            <div class="col col-xs-3"><div class="round-price"><span><strong><?=$quest['f4'];?></strong> руб.</span></div></div>
            <div class="col col-xs-9">
              <h3><?=$quest['name']?></h3>
              <p><?=htmlspecialchars_decode($quest['detail_text'])?></p>
            </div>
          </div>
          <div class="icons">
            <div class="col col-xs-4">
              <div class="time icon-mini">
                <p><i class="icon-wall-clock"></i>Время <strong><?=$quest['f1'];?></strong></p>
              </div>
            </div>
            <div class="col col-xs-4">
              <div class="group icon-mini">
                <p><i class="icon-multiple-users-silhouette"></i>Участников <strong><?=$quest['f2'];?></strong></p>
              </div>
            </div>
            <div class="col col-xs-4">
              <div class="distance icon-mini">
                <p><i class="icon-speedometer"></i>Растояние <strong><?=$quest['f3'];?></strong></p>
              </div>
            </div>
          </div>
        </div>
      </figure>
    </div>
    <?php  } ?>
  </div>
</div>
