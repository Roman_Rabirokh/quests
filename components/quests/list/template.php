<?php foreach($data['ITEMS'] as $item)
{ ?>


  <div class="col col-lg-4 col-sm-6">
    <figure class="icon-big" style="background-image:url(<?=$item['thumb']?>);    border: 2px solid #fff;
    border-image: url(../sprites/sprite_border.png) 2 stretch;
    border-radius: 3px;background-size:cover;">
      
      <div class="top">г.<?=$item['city'];?></div>
      <div class="center">
        <a class="btn btn-default btn-lg" href="<?=$item['url']?>">ВЫБРАТЬ КВЕСТ</a>
      </div>
      <div class="bottom">
        <div class="clearfix desc">
          <div class="col col-xs-3"><div class="round-price"><span><strong><?=$item['f4'];?></strong> руб.</span></div></div>
          <div class="col col-xs-9">
            <h3><?=$item['name'];?></h3>
            <p><?=htmlspecialchars_decode($item['detail_text']);?></p>
          </div>
        </div>
        <div class="icons">
          <div class="col col-xs-4">
            <div class="time icon-mini">
              <p><i class="icon-wall-clock"></i>Время <strong><?=$item['f1'];?></strong></p>
            </div>
          </div>
          <div class="col col-xs-4">
            <div class="group icon-mini">
              <p><i class="icon-multiple-users-silhouette"></i>Участников <strong><?=$item['f2'];?></strong></p>
            </div>
          </div>
          <div class="col col-xs-4">
            <div class="distance icon-mini">
              <p><i class="icon-speedometer"></i>Растояние <strong><?=$item['f3'];?></strong></p>
            </div>
          </div>
        </div>
      </div>
    </figure>
  </div>
  <?php } ?>
 <?php if($endPage>1) { ?>
  <div class="col col-xs-12" id="pagination">
    <ul class="pagination">
	
      <li <?php if($pages==$startPage) {?> class="disable" <? } ?> > <a href="<? if($pages!=$startPage){ $tmp=$pages-1; echo ("?page=".$tmp.$transportUrlPagination); } else echo "#";?>" class="icon-caret-prev"> </a></li>
      <?php for($i=$startPage; $i<=$endPage; $i++)
      { ?>  
	  <li <?php if($i==$pages) {?>  class="disable"  <? }?> ><a href="?page=<?echo($i.$transportUrlPagination)?>"><?=$i ?></a></li>
	  
   <?php  } ?> 
	  <li <?php if($pages==$endPage) {?> class="disable" <? } ?> ><a href="<?php $tmp=$pages+1; if($pages!=$endPage) { echo ("?page=".$tmp.$transportUrlPagination); } else echo "#"; ?>" class="icon-caret-next"> </a></li>
    </ul>
  </div>
 <?php } ?>
