<?php

$idCity = isset($params['cityId']) ? $params['cityId'] : false;
$questType = isset($params['questType']) ? $params['questType'] : false;
$transportId = !empty($params['transport']) ? $params['transport'] : false;


// Пагинация
$pages = isset($params['page']) ? $params['page'] - 1 : 0; // страница
$limit = 6; // количество отображаеммых квестов  на одной странице
$start = $limit * $pages;
$countPages = 0;
$transportUrlPagination = !empty($params['transport']) ? '&transport=' . $params['transport'] : '';


// выбрать квесты по транспорту, если он был задан
if ($transportId) {
    $transportId = ' AND cdata.f17 = ' . $transportId . ' ';
} else {
    $transportId = '';
}


if (!$idCity && !$questType) {
    $data['ITEMS'] = dbQuery('SELECT c.name, c.detail_text, c.mainimage, c.url, cdata.*, city.name AS city, c.created FROM #__content
    AS c
    INNER JOIN #__content_data_2
    AS cdata ON(c.id=cdata.id)
    INNER JOIN #__content
    AS city ON(cdata.f5=city.id)
    WHERE c.content_type = :cType AND c.active = :status ' . $transportId . '
    ORDER BY c.created DESC
    LIMIT :start , :end
  ', array(':cType' => 2, ':status' => 1, ':start' => $start, ':end' => $limit));
    $countPages = dbGetOne('SELECT COUNT(c.id) FROM #__content AS c
		INNER JOIN #__content_data_2
		AS cdata ON(c.id=cdata.id)
		WHERE  c.content_type = :cType AND c.active = :status ' . $transportId, array(':cType' => 2, ':status' => 1));
} else if (!$questType) {
    $data['ITEMS'] = dbQuery('SELECT c.name, c.detail_text, c.mainimage, c.url, cdata.*, city.name AS city, c.created FROM #__content
    AS c
    INNER JOIN #__content_data_2
    AS cdata ON(c.id=cdata.id)
    INNER JOIN #__content
    AS city ON(cdata.f5=city.id)
    WHERE c.content_type  = :cType AND c.active  = :status AND city.id  =  :cityId ' . $transportId . '
    ORDER BY c.created DESC
    LIMIT :start , :end
  ', array(':cType' => 2, ':status' => 1, ':cityId' => $idCity, ':start' => $start, ':end' => $limit));
    $countPages = dbGetOne('SELECT COUNT(c.id) FROM #__content  AS c
    INNER JOIN #__content_data_2
    AS cdata ON(c.id=cdata.id) 
	WHERE  c.content_type = :cType AND c.active = :status AND cdata.f5 = :cityId ' . $transportId . '', array(':cType' => 2, ':status' => 1, ':cityId' => $idCity));
} else {
    $data['ITEMS'] = dbQuery('SELECT c.name, c.detail_text, c.mainimage, c.url, cdata.*, city.name AS city, c.created FROM #__content
    AS c
    INNER JOIN #__content_data_2
    AS cdata ON(c.id=cdata.id)
    INNER JOIN #__content
    AS city ON(cdata.f5=city.id)
    INNER JOIN #__content
    AS property ON(cdata.f13=property.id)
    WHERE c.content_type  = :cType AND c.active  = :status AND property.id  =  :propertyId ' . $transportId . '
    ORDER BY c.created DESC
    LIMIT :start , :end 
  ', array(':cType' => 2, ':status' => 1, ':propertyId' => $questType, ':start' => $start, ':end' => $limit));
    $countPages = dbGetOne('SELECT COUNT(c.id) FROM #__content AS c
	  INNER JOIN #__content_data_2
	    AS cdata ON(c.id=cdata.id)
	    WHERE  c.content_type = :cType AND c.active = :status AND cdata.f13= :propertyId ' . $transportId . '', array(':cType' => 2, ':status' => 1, ':propertyId' => $questType));
}
foreach ($data['ITEMS'] as $key => &$item) {
    $item['thumb'] = getImageById($item['mainimage'], array('height' => 381, 'width' => 333));
    $item['url'] = Content::contentUrl($item['url']);
    $item['detail_text']= mb_substr($item['detail_text'], 0, 150, 'UTF-8').' ...';
}
unset($item);
// Пагинация, выборка отоборжаемых номеров страниц
$countViewPages = 5; // количество отображаемых страниц

$pages++; // прибавляем 1 для отображения.
if ($countPages > $limit)
    $countPages = ceil($countPages / $limit);
else
    $countPages = 1;



if (($pages + 2) >= $countPages) {
    $endPage = $countPages;
} else {
    if (($pages + 2) >= $countViewPages)
	$endPage = $pages + 2;
    else
	$endPage = $countViewPages;
}
if (($pages - 2) < 2) {
    $startPage = 1;
} else {
    $startPage = $pages - 2;
    if (($endPage - $startPage) < $countViewPages && (($pages - 2) > 2)) {
	$startPage = $endPage - 4;
    }
}


//print_r('<pre>');
//print_r($pages);
//print_r('</pre>');


include('template.php');
