<?php
$idCity = isset($params['cityId']) ? $params['cityId'] :  false;
$data['$transportId'] = !empty($params['transport']) ? $params['transport'] : false;
$data['CITIES'] = dbQuery('SELECT city.name, city.url, city.id FROM #__content
AS city
WHERE city.content_type = :type AND city.active = :status
',array(':type'=>3,'status'=>1));
foreach($data['CITIES'] as &$item)
{
  $item['url']  = Content::contentUrl($item['url']);
}
unset($item);
    
$data['transport']= dbQuery('SELECT id, name FROM #__content WHERE content_type = :type', array(':type'=> 6));    
$transportUrlPagination= !empty($params['transport']) ? '?transport='.$params['transport'] : '';
if($idCity)
  $data['nameCity'] = dbGetOne('SELECT name FROM #__content
    WHERE id  = :idCity',array(':idCity'=>$idCity));
if($data['$transportId'])
    $data['transportName']=dbGetOne('SELECT name FROM #__content
    WHERE id  = :idTransport',array(':idTransport' => $data['$transportId']));
else
    $data['transportName']='Любой транспорт';

include('template.php');
?>