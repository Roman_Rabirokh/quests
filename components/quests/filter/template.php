<div class="row navigation">
  <div class="col col-xs-12 one">
    <div class="center">
	 <div class="btn-group">
      <a class="btn btn-default btn-lg dropdown-toggle icon-small dropdown" type="button" data-toggle="dropdown" id="city" aria-haspopup="true" aria-expanded="false">
      <?php if(isset($data['nameCity'])) :
        echo $data['nameCity'];
      ?>
      <?php else : ?>
        ВАШ ГОРОД
      <?php endIf; ?>
      <span class="icon-caret"></span></a>
      <ul class="dropdown-menu" aria-labelledby="city">
        <?php
        foreach($data['CITIES'] as $city)
        { ?>
        <li><a href="<?echo($city['url'].$transportUrlPagination);?>"><?=$city['name'];?></a></li>
        <?php } ?>
      </ul>
	  </div>
      <a class="btn btn-default btn-lg icon-small" href="/">Все квесты</a>
      <a class="btn btn-default btn-lg icon-small" href="/popular/">ПОПУЛЯРНЫЕ</a>
      <a class="btn btn-default btn-lg icon-small" href="/new/">НОВИНКИ</a>
    </div>
  </div>
  <div class="col col-xs-12 gray">
    <div class="center">
	 
      <p class="no-marg">Вид транспорта:</p><div class="btn-group"> <a class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown" id="transport" aria-haspopup="true" aria-expanded="false"><?php echo $data['transportName']; ?> <span class="icon-caret"></span></a>
      <ul class="dropdown-menu" aria-labelledby="transport">
	 <li> <a href="?transport=0"> Любой транспорт</a></li>
	<?php foreach($data['transport'] as $transport) { ?>		    
	<li> <a href="?transport=<?php echo $transport['id'] ?>"> <?php echo $transport['name'] ?> </a></li>
					   <?php } ?>
	
	
      </ul>
	  </div>
    </div>
  </div>
</div>
