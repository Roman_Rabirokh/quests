<?php

$app->routes[] = array("pattern"=>'/publish\/quest/',"action"=>"_questPublish");

$app->routes[] = array("pattern"=>'/load\/level\/form/',"action"=>"_lodaLevelForm");

$app->routes[] = array("pattern"=>'/list\/quests/',"action"=>"_loadAdminListQuests");

$app->routes[] = array("pattern"=>'/save\/quest/',"action"=>"_saveQuest");

$app->routes[] = array("pattern"=>'/add\/quest/',"action"=>"_addQuest");

$app->routes[] = array("pattern"=>'/delete\/quest/',"action"=>"_deleteQuest");

$app->routes[] = array("pattern"=>'/delete\/image/',"action"=>"_deleteImage");

$app->routes[] = array("pattern"=>'/delete\/file/',"action"=>"_deleteFile");

$app->routes[] = array("pattern"=>'/delete\/invoice/',"action"=>"_deleteInvoice");

$app->routes[] = array("pattern"=>'/ajax\/save\/big-image/',"action"=>"_ajaxSaveBigImage");

$app->routes[] = array("pattern"=>'/ajax\/save\/image/',"action"=>"_ajaxMainSaveImage");


function _ajaxSaveBigImage() // TODO: объединить сохранения фото в одну функцию из _saveQuest?????
{
  global $app;

  $app->setStatus(TRUE,FALSE);
  $main_image=( isset($_FILES['file'])&& $_FILES['file']['size']>0) ? $_FILES['file'] : 0;
  $id= isset($_POST['id']) ? $_POST['id'] : 0;
  $id_old=dbGetOne('SELECT id FROM #__images
      WHERE parentid  = :id AND title="big_image"',array(':id' => $id));
      if($id_old)
        {
          $fileInfo = dbGetRow('SELECT * FROM #__images WHERE id = :id',array(':id'=>$id_old));
        	if(!empty($fileInfo))
        	{
        		unlink(_IMAGES_PATH . $fileInfo['filename']);
        		dbNonQuery('DELETE FROM #__images WHERE id = :id',array(':id'=>$id_old));
        	}
        }

      $id_image=uploadImage(1,$id,$main_image,'big_image');

      $new_image=getImageById($id_image);
      
      echo json_encode($new_image);
}

function _questPublish() // отправка на модерацию квеста
{
  global $app;
  global $user;
  $app->setStatus(TRUE,FALSE);
  if($user->Authorized()){
    $userID = $user->getID();
    $id = $_POST['id'];
    if($id){
    $check= dbGetOne('SELECT active FROM #__content WHERE id= :id AND userid= :userid', array(':id'=>$id, ':userid' => $userID));

    if(isset($check))
    {
	dbNonQuery('UPDATE #__content
        SET active = :active
        WHERE id  = :id',array(':active' => 2, ':id' => $id));
	echo json_encode(array('status'=>'success'));
    }
    
    }

    else
	echo json_encode(array('status'=>'error'));
  }else{
    echo json_encode(array('status'=>'error'));
  }
}
function _ajaxMainSaveImage() // TODO: объединить сохранения фото в одну функцию из _saveQuest
{
  global $app;

  $app->setStatus(TRUE,FALSE);
  $main_image=( isset($_FILES['file'])&& $_FILES['file']['size']>0) ? $_FILES['file'] : 0;
  $id= isset($_POST['id']) ? $_POST['id'] : 0;
  $id_old=dbGetOne('SELECT mainimage FROM #__content
      WHERE id  = :id',array(':id' => $id));
      if($id_old)
        {
          $fileInfo = dbGetRow('SELECT * FROM #__images WHERE id = :id',array(':id'=>$id_old));
        	if(!empty($fileInfo))
        	{
        		unlink(_IMAGES_PATH . $fileInfo['filename']);
        		dbNonQuery('DELETE FROM #__images WHERE id = :id',array(':id'=>$id_old));
        	}
        }

      $id_image=uploadImage(1,$id,$main_image);

      dbNonQuery('UPDATE #__content
        SET mainimage = :mainimage
        WHERE id  = :id',array(':mainimage' => $id_image, ':id' => $id));
      $new_image=getImageById($id_image,array('height'=>220, 'width' =>240));
      //print_r($new_image);
      echo json_encode($new_image);
}
function _deleteInvoice($params){
  global $app;
  global $user;

  if($user->Authorized()){
    $userID = $user->getID();
    $id = $_POST['qid'];

    // echo "<pre>";
    // print_r($qID)."\n";
    // print_r($userID)."\n";
    // echo "</pre>";

    $deletedQID = dbNonQuery('DELETE FROM #__invoices
      WHERE id = :id
      AND user_id = :user_id',
      array(':id'=>$id,
            ':user_id'=>$userID));
    
    $count=(int) dbGetOne('SELECT count(id) AS c FROM #__game_levels WHERE invoice_id = :id', array(':id' => $id ));
    for($i=0;$i<$count;$i++)
    {
      dbNonQuery('DELETE FROM #__game_levels
	WHERE invoice_id= :id',
	 array(':id'=>$id,
            ));
    }
    
    echo json_encode(array('status'=>1));

  }else{
    Header('Location: /');
  }
}

function _lodaLevelForm($params)
{
    global $app;

    $app->setStatus(TRUE,FALSE);

    $app->includeComponent('users/quest.level.form');
}

function _loadAdminListQuests($params)
{
  global $app;

  $app->setStatus(TRUE,FALSE);

  $app->includeComponent('users/list.created.quests');
}

function _saveQuest($params)
{
  global $app;

  $app->setStatus(TRUE,FALSE);
  $data = isset($_POST['questID'])  ? $_POST : 0;
//  if(empty($data['name_quest']))
//    exit('<script>alert("Не задано имя квеста")</script>');
//  if(empty($data['detail_text']))
//    exit('<script>alert("Не задано описание квеста")</script>');
//  if(empty($data['questID']))
//    exit('<script>alert("Нет ID квеста")</script>');
//  if(empty($data['time_quest']))
//    exit('<script>alert("Не задано среднее время квеста")</script>');
//  if(empty($data['people_quest']))
//    exit('<script>alert("Не задано кол-во участников квеста")</script>');
//  if(empty($data['length_quest']))
//    exit('<script>alert("Не задано растояние для квеста")</script>');
//  if(empty($data['price_quest']))
//    exit('<script>alert("Не задана цена для квеста")</script>');
//  if(empty($data['location']))
//    exit('<script>alert("Не задано как добраться до квеста")</script>');
//  if(empty($data['start_route_quest']))
//    exit('<script>alert("Не задано начало маршрута для квеста")</script>');
//
//  if(empty($data['time_start']))
//    exit('<script>alert("Не задано время игры: от для квеста")</script>');
//  if(empty($data['time_end']))
//    exit('<script>alert("Не задано время игры: до для квеста")</script>');

//  foreach($data['level'] as &$level)
//  {
//    if(empty($level['name']))
//      exit('<script>alert("Не задано название уровня")</script>');
//    if(empty($level['task']))
//      exit('<script>alert("Не задано задание для уровня")</script>');
//    if(empty($level['answer']))
//      exit('<script>alert("Не задан ответ для уровня")</script>');
//    else
//        $level['answer']=mb_strtolower($level['answer']);
//  }
//  unset($level);
$url_name="";
  if($data)
  {
    $url_name=$url_name_def=create_urlname($data['name_quest']);
    $isNameUrl=dbGetOne('SELECT id FROM #__content
      WHERE url LIKE :url', array(':url'  =>  $url_name));
      $index=1;
      while($isNameUrl)
      {
        $url_name=$url_name_def.'-'.$index;
        $isNameUrl=dbGetOne('SELECT id FROM #__content
          WHERE url LIKE :url', array(':url'  =>  $url_name));
        $index++;
      }

    dbNonQuery('UPDATE #__content
      SET updated = NOW(), name = :name, detail_text  = :detail_text, active = :active, urlname =  :urlname, url = :url
      WHERE id  = :id ',
      array(':name'  =>  $data['name_quest'], ':detail_text' => $data['detail_text'],':id' =>  $data['questID'],
      ':active' => 0 , ':urlname' => $url_name,':url' => $url_name)  , TRUE);


        dbNonQuery('UPDATE #__content_data_2
          SET f1 = :time_quest, f2 = :people_quest, f3  = :length_quest,  f4  = :price, f6  = :location, f7  = :start_route_quest,
           f9  = :time_start, f10  = :tools, f15  = :time_end , f16  = :date, f17 = :transport,
          f5 = :city
          WHERE id  = :id ',
          array(':time_quest'  =>  $data['time_quest'], ':people_quest' =>$data['people_quest'], ':length_quest' => $data['length_quest'],
          ':price' => $data['price_quest'], ':location' => $data['location'], ':start_route_quest' => $data['start_route_quest'],
          ':time_start' => $data['time_start'], ':tools' => $data['tools'],
          ':time_end' => $data['time_end'], ':id' =>$data['questID'], ':date' => $data['date'], ':transport' => $data['transport'],
          ':city' => $data['city']));

    $temp_levels  = dbQuery('SELECT id FROM #__quest_levels
      WHERE quest_id  = :quest_id
      ORDER BY level', array(':quest_id'  =>$data['questID']));
    $count=1;
    $levels=array();
    $image_levels= isset($_FILES['level']) ? $_FILES['level'] : 0;
    foreach($temp_levels as $lev)
    {
      array_push($levels,(int)$lev['id']);
    }
    foreach($data['level'] as $key => $level)
    {
	$id_old=dbGetOne('SELECT image FROM #__quest_levels
		    WHERE id  = :id',array(':id' => $key));

	if(!empty($image_levels['size'][$key])){
	    $image_tmp['name']=$image_levels['name'][$key];
	    $image_tmp['type']=$image_levels['type'][$key];
	    $image_tmp['tmp_name']=$image_levels['tmp_name'][$key];
	    $image_tmp['error']=$image_levels['tmp_name'][$key];
	    $image_tmp['size']=$image_levels['size'][$key];
	    if($id_old)
		      {
			$fileInfo = dbGetRow('SELECT * FROM #__images WHERE id = :id',array(':id'=>$id_old));
			      if(!empty($fileInfo))
			      {
				      unlink(_IMAGES_PATH . $fileInfo['filename']);
				      dbNonQuery('DELETE FROM #__images WHERE id = :id',array(':id'=>$id_old));
			      }
		      }
	    $id=uploadImage(2,$key,$image_tmp);
     
	}
	else{ 
	    if(!empty($id_old))
		$id=$id_old;
	    else
		$id=0;
	}
      if(array_search($key,$levels)!==FALSE)
      {
	  
        dbNonQuery('UPDATE #__quest_levels
          SET name = :name, task = :task, hint_1  = :hint_1,  hint_2  = :hint_2, hint_3  = :hint_3, answer  = :answer,
          quest_id  = :quest_id, time_hint_1	 = :time_hint_1, time_hint_2	 = :time_hint_2, time_hint_3  = :time_hint_3, image = :image
          WHERE id  = :id ',
          array(':name'  =>  $level['name'], ':task' =>$level['task'], ':hint_1' => $level['hint_1'],
          ':hint_2' => $level['hint_2'], ':hint_3' => $level['hint_3'], ':answer' => $level['answer'],
          ':quest_id' => $data['questID'], ':time_hint_1' => $level['time_hint_1'],
          ':time_hint_2' => $level['time_hint_2'], ':time_hint_3' => $level['time_hint_3'], ':id' =>  $key, ':image' => $id));
          

	  unset($levels[array_search($key, $levels)]);
      }
      else
      {
        dbNonQuery('INSERT INTO #__quest_levels
          (name, task, level, hint_1, hint_2, hint_3, answer, quest_id, time_hint_1, time_hint_2, time_hint_3, image  )
          VALUES (:name, :task, :level, :hint_1, :hint_2, :hint_3, :answer, :quest_id, :time_hint_1, :time_hint_2, :time_hint_3, :image) ',
          array(':name'  =>  $level['name'], ':task' => $level['task'], ':level' => $count,
          ':hint_1' => $level['hint_1'], ':hint_2' => $level['hint_2'], ':hint_3' => $level['hint_3'],
          ':answer' => $level['answer'], ':quest_id' => $data['questID'],
          ':time_hint_1' => $level['time_hint_1'],
          ':time_hint_2' => $level['time_hint_2'], ':time_hint_3' => $level['time_hint_3'], ':image'=>$id));
      }
      $count++;
    }
    foreach($levels as $value)
    {
      dbNonQuery('DELETE FROM #__quest_levels
      WHERE id  = :id', array(':id' => $value));
    }

    $file_image=( isset($_FILES['file_image']) && $_FILES['file_image']['size']>0) ? $_FILES['file_image'] : 0;
    $file_sound=( isset($_FILES['file_sound'])&& $_FILES['file_sound']['size']>0) ? $_FILES['file_sound'] : 0;
    $main_image=( isset($_FILES['main_image'])&& $_FILES['main_image']['size']>0) ? $_FILES['main_image'] : 0;
    if($file_image)
    {
      $id=uploadImage(1,$data['questID'],$file_image,'image_additional');
      dbNonQuery('UPDATE #__images
        SET showorder = :showorder
        WHERE id  = :id',array(':showorder'  =>  501, ':id' => $id));

    }
    if($file_sound)
    {
      $id=uploadFile($data['questID'],$file_sound['name'],$file_sound['tmp_name']);
      print_r($id);
    }
    if($main_image)
    {
      $id_old=dbGetOne('SELECT mainimage FROM #__content
      WHERE id  = :id',array(':id' => $data['questID']));;
      if($id_old)
        {
          $fileInfo = dbGetRow('SELECT * FROM #__images WHERE id = :id',array(':id'=>$id_old));
        	if(!empty($fileInfo))
        	{
        		unlink(_IMAGES_PATH . $fileInfo['filename']);
        		dbNonQuery('DELETE FROM #__images WHERE id = :id',array(':id'=>$id_old));
        	}
        }

      $id=uploadImage(1,$data['questID'],$main_image);

      dbNonQuery('UPDATE #__content
        SET mainimage = :mainimage
        WHERE id  = :id',array(':mainimage' => $id, ':id' => $data['questID']));

    }

  }
  
    // переход на страницу с созданым квестом
//    if($data['active']==1)
//	redirect('http://'.$_SERVER['HTTP_HOST'].'/'.$url_name);
//    else
    if($data['no_ajax']=='1')
        redirect('http://'.$_SERVER['HTTP_HOST'].'/personal/created-quests/');

}

function _addQuest($params)
{
  global $app;
  global $user;
  $app->setStatus(TRUE,TRUE);
  if($user->Authorized())
  {
  $id = dbNonQuery('INSERT INTO #__content
    (created, content_type, active, showorder, protected, pagesize, userid)
    VALUES (NOW(), :contentType, :active, :showorder, :protected, :pageSize, :userId) ',
    array(':contentType'  =>  2,  ':active' =>  0,  ':showorder'  => 500, ':protected'  =>  0,  ':pageSize' => 0, ':userId' => $user-> getID()), TRUE);
  
  dbNonQuery('INSERT INTO #__content_data_2
      (id)
      VALUES (:id)',
      array(':id'  =>  $id), TRUE);
  // создать 1 уровень
  dbNonQuery('INSERT INTO #__quest_levels
      (quest_id, level)
      VALUES (:id, :level)',
      array(':id'  =>  $id, ':level' => 1), TRUE);

  header('Location:http://'.$_SERVER['HTTP_HOST'].'/edit/quest?id='.$id);
  }
  else
  {
      redirect('/registration/');
  }
}

function _deleteQuest($params)
{
  global $app;

  $app->setStatus(TRUE,FALSE);

  $id = isset($_POST['id'])  ? $_POST['id'] : 0;

  if($id)
  {
    dbNonQuery('DELETE FROM #__content
    WHERE id  = :id', array(':id' => $id));

    dbNonQuery('DELETE FROM #__content_data_2
    WHERE id  = :id', array(':id' => $id));

    dbNonQuery('DELETE FROM #__quest_levels
    WHERE quest_id  = :id', array(':id' => $id));
  }

}


function _deleteImage($params)
{
  global $app;

  $app->setStatus(TRUE,FALSE);

  $id = isset($_POST['id'])  ? $_POST['id'] : 0;
  if($id)
  {
    $fileInfo = dbGetRow('SELECT * FROM #__images WHERE id = :id',array(':id'=>$id));
  	if(!empty($fileInfo))
  	{
  		unlink(_IMAGES_PATH . $fileInfo['filename']);
  		dbNonQuery('DELETE FROM #__images WHERE id = :id',array(':id'=>$id));
    }
  }
}

function _deleteFile($params)
{
  global $app;

  $app->setStatus(TRUE,FALSE);

  $id = isset($_POST['id'])  ? $_POST['id'] : 0;
  if($id)
  {
    $fileInfo = dbGetRow('SELECT * FROM #__files WHERE id = :id',array(':id'=>$id));
  	if(!empty($fileInfo))
  	{
  		unlink(_FILES_PATH . $fileInfo['filename']);
  		dbNonQuery('DELETE FROM #__files WHERE id = :id',array(':id'=>$id));
  	}

  }
}
