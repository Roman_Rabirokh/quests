<?php

//$app->routes[] = array("pattern"=>'/send\/answer/',"action"=>"_sendAnswer");
$app->routes[] = array("pattern" => '/game\/check-answer/', "action" => "_checkAnswer");
$app->routes[] = array("pattern" => '/game\/show-answer/', "action" => "_gameShowAnswer");
$app->routes[] = array("pattern" => '/game\/next-level/', "action" => "_gameNextLevel");
$app->routes[] = array("pattern" => '/game\/stop-game/', "action" => "_gameStop");
$app->routes[] = array("pattern" => '/game\/accept-hint/', "action" => "_gameAcceptHint");
$app->routes[] = array("pattern" => '/game\/check\-code\//', "action" => "_gameCheckCode");
$app->routes[] = array("pattern" => '/game\/buy\?idInvoice=\d+/', "action" => "_gameBuy");
$app->routes[] = array("pattern" => '/game\/get\-code\//', "action" => "_gameGetCode");
$app->routes[] = array("pattern" => '/create\/invoice/', "action" => "_createInvoice");
$app->routes[] = array("pattern" => '/game\/start\?id=\d+/', "action" => "_startGame");
// adding new game and redirect to personal cabinet function for authorized users
$app->routes[] = array("pattern" => '/add\/game\//', "action" => "_addGame");
$app->routes[] = array("pattern" => '/game\/add\/rating/',"action"=>"_addRating");



function _addRating()
{
    global $user;
    global $app;
    $idUser=$user->getID();
    $idInvoice=!empty($_POST['id-invoice']) ?(int) $_POST['id-invoice'] : 0 ;
    $rating= !empty($_POST['rating']) ? (int)$_POST['rating'] : 0;
    if($rating>10)
        $rating=10;
    if($rating<0)
        $rating=1;
    
    if($idInvoice)
    {
        $idInvoice= dbGetOne('SELECT id FROM #__invoices WHERE id= :id AND user_id= :user AND status=3', [':id' => $idInvoice, ':user' => $idUser]);
        if(!empty($idInvoice))
        {    
            dbNonQuery('UPDATE #__invoices  SET rating= :rating  WHERE id= :id', [':id'=> $idInvoice, ':rating' => $rating]);
            // обновить рейтинг в квестах
            $id= dbGetOne('SELECT quest_id FROM #__invoices WHERE  id= :id', [':id'=> $idInvoice]);
            $rating= (int)dbGetOne('SELECT AVG(rating) FROM #__invoices WHERE 	quest_id= :id AND rating>0', [':id' => $id]);
            dbNonQuery('UPDATE #__content SET rating = :rating WHERE id= :id', [':rating' => $rating, ':id' => $id]);
            redirect($_SERVER['HTTP_REFERER']);
        }
    }
}
function _gameStop() {
    $idGame = (!empty($_POST['idGame'])) ? $_POST['idGame'] : 0;
    $level = (!empty($_POST['level'])) ? $_POST['level'] : 0;
    // TODO:check isset level
    dbNonQuery('UPDATE #__game_levels
        SET  time_hint_1= NULL, time_hint_2 = NULL, end = NULL, start = null
        WHERE invoice_id = :invoice_id AND number_level= :number_level', array(
	':invoice_id' => $idGame, ':number_level' => $level,
    ));
    exit(json_encode(array('status' => 'success')));
}

// проверка ответа
function _checkAnswer() {
    $idGame = (!empty($_POST['idGame'])) ? $_POST['idGame'] : 0;
    $level = (!empty($_POST['level'])) ? $_POST['level'] : 0;
    $answer = (!empty($_POST['answer'])) ? mb_strtolower(trim($_POST['answer'])) : 0;
    $offset_time = (isset($_SESSION['time_zone'])) ? $_SESSION['time_zone'] : +2;

    $date = new DateTime("now", new DateTimeZone('UTC'));
    $date->add(new DateInterval('PT' . $offset_time . 'H'));
    $date = $date->format('Y-m-d H:i:s');


    $answersLevel = getAnswerLevel($idGame, $level);
    $answersLevel = explode(',', $answersLevel);
    
    foreach ($answersLevel as $answerLevel) {
        $answerLevel=trim($answerLevel);
        $answerLevel=mb_strtolower($answerLevel);
	if (strnatcasecmp($answer, $answerLevel) == 0) {
	    
		dbNonQuery('UPDATE #__game_levels
		SET end= :time, success_level = 1
		WHERE invoice_id = :invoice_id AND number_level= :number_level', array(
		    ':invoice_id' => $idGame, ':number_level' => $level, ':time' => $date,
		));
	    if (!isNextLevelNoStart($idGame)){
		// игра закончена
		dbNonQuery('UPDATE #__invoices 
			SET status = 3 WHERE  id =:id', array(':id' => $idGame));
		exit(json_encode(array('status' => 'end')));
	    }
	  exit(json_encode(array('status' => 'success')));
	}
    }


    exit(json_encode(array('status' => 'wrong')));
}

function _gameShowAnswer() {
    $idGame = (!empty($_POST['idGame'])) ? $_POST['idGame'] : 0;
    $level = (!empty($_POST['level'])) ? $_POST['level'] : 0;
    //TODO: проверку сделать на то все ли правильно по входящим данным
    $offset_time = (isset($_SESSION['time_zone'])) ? $_SESSION['time_zone'] : +2;

    $date = new DateTime("now", new DateTimeZone('UTC'));
    $date->add(new DateInterval('PT' . $offset_time . 'H'));
    $date->add(new DateInterval('PT15M'));
    $date = $date->format('Y-m-d H:i:s');

    $answer = getAnswerLevel($idGame, $level);
    $answersLevel = explode(',', $answer);
    dbNonQuery('UPDATE #__game_levels
	    SET end= :time, success_level = 1
	    WHERE invoice_id = :invoice_id AND number_level= :number_level', array(
	':invoice_id' => $idGame, ':number_level' => $level, ':time' => $date,
    ));
    if (!isNextLevelNoStart($idGame)) // игра закончена
	dbNonQuery('UPDATE #__invoices 
			SET status = 3 WHERE  id =:id', array(':id' => $idGame));
    exit(json_encode(array('answer' => $answersLevel[0], 'next' => isNextLevelNoStart($idGame))));
}

function _gameNextLevel() {
    $idGame = (!empty($_POST['idGame'])) ? $_POST['idGame'] : 0;
    $level = (!empty($_POST['level'])) ? $_POST['level'] : 0;
    $offset_time = (isset($_SESSION['time_zone'])) ? $_SESSION['time_zone'] : +2;
    //TODO: проверку сделать на то все ли правильно по входящим данным
    $level++;
    $date = new DateTime("now", new DateTimeZone('UTC'));
    $date->add(new DateInterval('PT' . $offset_time . 'H'));
    $date = $date->format('Y-m-d H:i:s');
    // TODO:  добавить в следующий уровень дату старта
    dbNonQuery('UPDATE #__game_levels
        SET  start= :time
        WHERE invoice_id = :invoice_id AND number_level= :number_level', array(
	':invoice_id' => $idGame, ':number_level' => $level, ':time' => $date,
    ));
    exit(json_encode($date));
    //redirect('/game?id='.$idGame);
}

//
function _gameAcceptHint() {
    $idGame = (!empty($_POST['idGame'])) ? $_POST['idGame'] : 0;
    $level = (!empty($_POST['level'])) ? $_POST['level'] : 0;
    $idHint = (!empty($_POST['idHint'])) ? $_POST['idHint'] : 0;
    //TODO: проверку сделать на обновление

    $offset_time = (isset($_SESSION['time_zone'])) ? $_SESSION['time_zone'] : +2;

    $date = new DateTime("now", new DateTimeZone('UTC'));
    $date->add(new DateInterval('PT' . $offset_time . 'H'));
    $date = $date->format('Y-m-d H:i:s');
    if ($idHint && $level && $idGame) {
	$hint = ' time_hint_' . $idHint . ' ';

	dbNonQuery('UPDATE #__game_levels
	    SET ' . $hint . '= :time
	    WHERE invoice_id = :invoice_id AND number_level= :number_level', array(
	    ':invoice_id' => $idGame, ':number_level' => $level, ':time' => $date,
	));
    }
    exit(json_encode($date));
}

// получение кода для передачи игры
function _gameGetCode() {
    global $app;
    global $user;
    $idGame = !empty($_POST['idGame']) ? $_POST['idGame'] : false;
    //print_r($idGame);
    if ($idGame) {
	$statusGame = dbGetOne('SELECT status FROM #__invoices WHERE id = :id AND user_id = :user', array(':id' => $idGame, ':user' => $user->getID()));

	if ($statusGame == 1) {
	    $code = $idGame . generateCode();
	    dbNonQuery('UPDATE #__invoices
		SET secret_code = :code
		WHERE id = :invoice_id', array(
		':invoice_id' => $idGame,
		':code' => $code
	    ));
	    exit(json_encode($code));
	} else
	    exit();
    }
    else {
	exit();
    }
}

function _gameBuy() {
    global $app;
    global $user;
    $idInvoice = !empty($_GET['idInvoice']) ? $_GET['idInvoice'] : false;
    $invoice = dbGetRow('SELECT u.percent, i.price FROM #__users AS u '
	    . 'INNER JOIN #__content AS c ON(c.userid=u.id)'
	    . 'INNER JOIN #__invoices AS i ON(i.quest_id=c.id) '
	    . 'WHERE i.id= :id', array(':id' => $idInvoice));
    if (!empty($invoice['percent'])) {
	$percent = ($invoice['percent'] / 100) * $invoice['price'];
    } else {
	$percent = 0;
    }
    if ($idInvoice) {
	dbNonQuery('UPDATE #__invoices
        SET date_pay = NOW(), status = 1, sum_paid_user= :sum
        WHERE id = :invoice_id', array(
	    ':invoice_id' => $idInvoice,
	    ':sum' => $percent,
	));
	redirect('/personal/');
    } else {
	redirect('/');
    }
}

// general function to validate data from form inputs
function validation($input) {
    $input = trim($input);
    $input = stripslashes($input);
    $input = htmlspecialchars($input);
    return $input;
}

function _gameCheckCode() {

    global $user;
    global $app;
    $app->setStatus(TRUE, FALSE);
    $code = !empty($_POST['code']) ? $_POST['code'] : false;
    if ($code) {
	$dataInvoice = dbGetRow('SELECT id, user_id FROM #__invoices WHERE secret_code = :code', array(':code' => $code));

	if ($dataInvoice['id']) {
//	print_r($dataInvoice);
//        exit();
	    dbNonQuery('UPDATE #__invoices
        SET  secret_code = 0 , user_id = :user_id , gift_from_id = :from
        WHERE id = :invoice_id', array(
		':invoice_id' => $dataInvoice['id'], ':user_id' => $user->getID(), ':from' => $dataInvoice['user_id']
	    ));
	    redirect('/personal/');
	} else
	    redirect('/personal/');
    } else
	redirect('/personal/');
}

// кнопка Начать играть на странице квеста создание invoice происходит по ajax
function _addGame($params) {

    global $app;
    global $user;
    $app->setStatus(TRUE, FALSE);

    if ($user->Authorized()) {

	if (!empty($_POST['id'])) {
	    $questID = validation($_POST['id']);
	    $userID = $user->getID();
	    $questData = dbGetRow('SELECT content.id, data.f4 as price, u.percent
        FROM #__content
          AS content
        INNER JOIN #__content_data_2
          AS data
          ON content.id = data.id
	LEFT OUTER JOIN #__users 
	   AS u 
	   ON u.id = content.userid
        WHERE content.content_type = 2
        AND content.id = :id', array(':id' => $questID));
//      $invoiceID = dbGetOne('SELECT id
//        FROM #__invoices
//        WHERE user_id = :user_id
//        AND quest_id = :quest_id',
//        array(
//          ':user_id'=>$userID,
//          ':quest_id'=>$questID
//        ));   
//      if(!empty($questData) && empty($invoiceID)){
	    if(!empty($questData['percent']))
		$percent=($questData['percent']/100)*$questData['price'];
	    else {
		$percent=0;
	    }
	    $invoiceID = dbNonQuery('INSERT INTO #__invoices
          (user_id, price, date_create, quest_id, sum_paid_user)
          VALUES(:user_id, :price, NOW(), :quest_id, :sum)', array(
		':user_id' => $userID,
		':price' => $questData['price'],
		':quest_id' => $questData['id'],
		':sum' => $percent,
		    ), true);
//      }else if(!empty($questData) && !empty($invoiceID)){
//
//        dbNonQuery('UPDATE #__invoices
//        SET date_create = NOW()
//        WHERE id = :invoice_id',
//        array(
//          ':invoice_id'=>$invoiceID
//        ));
//      }
	    $quest = dbGetRow('SELECT data.*, count(levels.id) AS count   FROM #__content_data_2
                    AS data
                    INNER JOIN #__quest_levels
                    AS levels ON (data.id = levels.quest_id)
                    WHERE data.id = :id', array(':id' => $questID));
	    for ($i = 0; $i < $quest['count']; $i++) {
		dbNonQuery('INSERT INTO #__game_levels
		(invoice_id, number_level, success_level)
		VALUES (:invoice_id, :number_level, :success_level)', array(':invoice_id' => $invoiceID, ':number_level' => $i + 1, ':success_level' => 0), TRUE);
	    }
	    exit(json_encode($invoiceID));
	    //redirect('/personal/');
	} else {
	    redirect('/personal/');
	}
    } else {
	redirect('/registration/');
    }
}

// Кнопка скрытой формы на квестах
function _createInvoice($params) {
    global $app;

    $app->setStatus(TRUE, FALSE);

    global $user;

    $idUser = $user->getID();

    if (empty($idUser))
	redirect('/registration/');

    $id = isset($_POST['id']) ? (int) $_POST['id'] : 0;
    $quest = dbGetRow('SELECT data.*, count(levels.id) AS count   FROM #__content_data_2
                    AS data
                    INNER JOIN #__quest_levels
                    AS levels ON (data.id = levels.quest_id)
                    WHERE data.id = :id', array(':id' => $id));
    $idInvoice = dbNonQuery('INSERT INTO #__invoices
                        (user_id, status, price, date_create, quest_id)
                        VALUES (:user_id, :status, :price, NOW(), :quest_id) ', array(':user_id' => $idUser, ':status' => 0, ':quest_id' => $id, ':price' => $quest['f4']), TRUE);
//print_r($quest);
    for ($i = 0; $i < $quest['count']; $i++)
	dbNonQuery('INSERT INTO #__game_levels
    (invoice_id, number_level, success_level)
    VALUES (:invoice_id, :number_level, :success_level)', array(':invoice_id' => $idInvoice, ':number_level' => $i + 1, ':success_level' => 0), TRUE);

    redirect('/personal/');
}

function _startGame($params) {
    global $app;

    $app->setStatus(TRUE, FALSE);

    global $user;

    $idUser = $user->getID();

    $offset_time = (isset($_SESSION['time_zone'])) ? $_SESSION['time_zone'] : +2;

    $date = new DateTime("now", new DateTimeZone('UTC'));
    $date->add(new DateInterval('PT' . $offset_time . 'H'));
    $date = $date->format('Y-m-d H:i:s');

    $idInvoice = (isset($_GET['id'])) ? (int) $_GET['id'] : 0;

    $isGame = dbGetRow('SELECT i.id AS id, g.id AS levelId, g.number_level
  FROM #__invoices
  AS i
  INNER JOIN #__users
  AS u ON(i.user_id = u.id)
  INNER JOIN #__content
  AS q ON (q.id = i.quest_id)
  INNER JOIN #__game_levels
  AS g ON(i.id  = g.invoice_id)
  WHERE q.content_type  =  :type AND u.id =  :id AND g.end IS NULL AND i.id = :idInvoice
  ORDER BY(g.number_level)'
	    , array(':type' => 2, ':id' => $idUser, ':idInvoice' => $idInvoice));
//print_r($isGame);
    if ($isGame['id']) {
	dbNonQuery('UPDATE #__game_levels
      SET start = :date
      WHERE id  = :id ', array(':date' => $date, ':id' => $isGame['levelId']));

	dbNonQuery('UPDATE #__invoices 
	     SET status = 2
	     WHERE id= :id', array(':id' => $isGame['id']));


	redirect('/game?id=' . $idInvoice);
    } else {
	redirect('/');
    }
}

// генерирует часть секретного кода.
function generateCode() {
    $size = 5;
    $code = '';
    for ($i = 0; $i <= $size; $i++)
	$code .= rand(0, 10);

    return $code;
}

function getAnswerLevel($idGame, $level) {
    $answer = dbGetOne('SELECT q_l.answer FROM #__quest_levels AS q_l
			INNER JOIN  #__content
			AS c ON (c.id = q_l.quest_id)
			INNER JOIN #__invoices AS i ON (i.quest_id=c.id)
			INNER JOIN #__game_levels AS g_l ON (g_l.invoice_id=i.id)
			WHERE i.id= :game AND g_l.number_level= :number AND  q_l.level= :number2', array(':game' => $idGame, ':number' => $level, ':number2' => $level));
    return $answer;
}

function isNextLevelNoStart($idGame) {
    $level = dbGetOne('SELECT g_l.number_level  FROM #__game_levels AS g_l
	     INNER JOIN #__invoices AS i ON(i.id=g_l.invoice_id)
	     WHERE i.id= :game AND g_l.success_level= 0 AND (g_l.start IS NULL OR  g_l.start="0000-00-00 00:00:00") AND (g_l.end IS NULL OR  g_l.end="0000-00-00 00:00:00") ', array(':game' => $idGame));
    if (empty($level))
	return false;
    else
	return true;
}

// возвращает потраченое время на игру
function getTimeGame($id) {
    $dateStart = dbGetOne('SELECT e.start FROM #__game_levels  AS e WHERE e.invoice_id= :id AND e.number_level=(SELECT MIN(g.number_level) FROM #__game_levels AS g WHERE g.invoice_id= :gid)', array(':id' => $id,'gid' => $id));
    $dateEnd = dbGetOne('SELECT e.end FROM #__game_levels AS e  WHERE e.invoice_id= :id  AND e.number_level=(SELECT MAX(g.number_level) FROM #__game_levels AS g WHERE g.invoice_id= :gid)', array(':id' => $id,'gid' => $id));
    
    $dateStart = new DateTime($dateStart, new DateTimeZone('UTC'));
    $dateEnd = new DateTime($dateEnd, new DateTimeZone('UTC'));
    
    $interval = $dateStart->diff($dateEnd);
    $diff = ($interval->format('%a')*1440)+($interval->format('%h')*60)+($interval->format('%i'));
    //$diff = $dateStart->format('Y-m-d-h-i');
    return($diff);
}
