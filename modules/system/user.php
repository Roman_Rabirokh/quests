<?php if(!defined("_APP_START")) { exit(); }

$app->routes[] = array("pattern"=>'/login\/form/',"action"=>"_showLoginFormAjax","contentType"=>"component");
$app->routes[] = array("pattern"=>'/registration\/form/',"action"=>"_showRegistrationFormAjax","contentType"=>"component");
$app->routes[] = array("pattern"=>'/login\/$/',"action"=>"_showLoginForm");
$app->routes[] = array("pattern"=>'/logout\/$/',"action"=>"_Logout");
$app->routes[] = array("pattern"=>'/registration\/(.*)/',"action"=>"_showRegistrationForm");
$app->routes[] = array("pattern"=>'/personal\/info/', "action"=>"_showPersonalInfo");
$app->routes[] = array("pattern"=>'/personal\/created\-quests/', "action"=>"_showCreatedQuests");
$app->routes[] = array("pattern"=>'/personal/',"action"=>"_showPersonalPage");
$app->routes[] = array("pattern"=>'/registration\/create/',"action"=>"_registrationCreate");
$app->routes[] = array("pattern"=>'/ajax\/check\/password/',"action"=>"_checkPassword");
$app->routes[] = array("pattern"=>'/user\/save\/info/',"action"=>"_saveInfo");

function _showCreatedQuests()
{
    global $app;
    global $page;
    global $user;
    $app->actionResult = TRUE;
    if ($user->Authorized()) {
	$page->meta_title = "Созданные квестов";
	include(_TEMPL . "created-quests.php");
    } else {
	Header("Location:/");
	exit();
    }
}

function _saveInfo()
{
    global $app;
    global $page;
    global $user;
    $app->actionResult = TRUE;
    if ($user->Authorized()) {
	$data['passw']=!empty($_POST['new_pass']) ?  md5($_POST['new_pass']._PASSWORD_SOLR): false;
	$data['login']=!empty($_POST['login']) ?  $_POST['login']: false;
	$data['name']=!empty($_POST['name']) ?  $_POST['name']: false;
	$data['surname']=!empty($_POST['surname']) ?  $_POST['surname']: false;
	$data['email']=!empty($_POST['email']) ?  $_POST['email']: false;
	$data['telephone']=!empty($_POST['telephone']) ?  $_POST['telephone']: false;
	$data['city']=!empty($_POST['city']) ?  $_POST['city']: false;
	print_r('<pre>');
	print_r($data);
	print_r('</pre>');
	foreach($data as $key => $value)
	{
	    print_r('<pre>');
	print_r($key);
	print_r('</pre>');
	print_r('<pre>');
	print_r($value);
	print_r('</pre>');
	print_r('<pre>');
	print_r('_________________');
	print_r('</pre>');
	    if($value)
		dbNonQuery('UPDATE #__users
		    SET '.$key.' = :value
		    WHERE id  = :id ',
		    array(':value'  =>  $value, ':id' => $user->getID())  , TRUE);
	}
	redirect('/personal/');
    } else {
	Header("Location:/");
	exit();
    }
}

function _checkPassword()
{
    global $app;
    global $page;
    global $user;
    $app->actionResult = TRUE;
    if ($user->Authorized()) {
	$pass=!empty($_POST['password']) ?  $_POST['password']: false;
	if($pass)
	{   
	    $pass=md5($pass._PASSWORD_SOLR);
    
	    $current_pass= dbGetOne('SELECT passw FROM #__users WHERE id= :id', array(':id'=> $user->getID()));
	    if(strcmp($pass,$current_pass)==0)
		    exit(json_encode (array('status' => 'Success')));
	    else
		exit(json_encode (array('status' => 'Error')));
	    
	}
	else
	{
	    // пароль не пришел.
	}
    } else {
	Header("Location:/");
	exit();
    }
}
function _showPersonalInfo() {
    global $app;
    global $page;
    global $user;
    $app->actionResult = TRUE;
    if ($user->Authorized()) {
	$page->meta_title = "Персональные данные";
	include(_TEMPL . "personal-info.php");
    } else {
	Header("Location:/");
	exit();
    }
}

function _showLoginForm()
{
	 global $app;
	 global $page;
	 $app->actionResult = TRUE;
	 $page->meta_title = "Авторизация";
	 $app->IncludeComponent("system/auth");
}
function _showLoginFormAjax()
{
	global $app;
	$app->loadTemplate = FALSE;
	$app->actionResult = TRUE;
	$app->IncludeComponent("system/auth");
}
function _showRegistrationForm()
{
	 global $app;
	 global $page;
	 $app->actionResult = TRUE;
	 $page->meta_title = "Регистрация";
	 $app->IncludeComponent("system/register");
}
function _showRegistrationFormAjax()
{
	global $app;
	$app->loadTemplate = FALSE;
	$app->actionResult = TRUE;
	$app->IncludeComponent("system/register");
}
function _showPersonalPage()
{
	 global $app;
	 global $page;
	 global $user;
	 $app->actionResult = TRUE;
	 if($user->Authorized()){
		 $page->meta_title = "Персональный кабинет";
		 include(_TEMPL . "personal.php");
	 }else{
		 Header("Location:/");
		 exit();
	 }
}
function _registrationCreate()
{
	$app->loadTemplate = FALSE;
	$app->actionResult = TRUE;
}
function _Logout()
{
	global $user;
	$user->Logout();
	redirect(APP::getServerProtocol() .  "://" . _SITE);
}
